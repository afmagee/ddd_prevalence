\documentclass[11pt]{article}
\usepackage{amsmath, amsfonts, amssymb, graphicx, mathrsfs}
\usepackage[round]{natbib}
\usepackage[margin=1in]{geometry}
\usepackage[usenames,dvipsnames,svgnames,table]{xcolor}
\usepackage{rotating}
\usepackage{rotfloat}
\usepackage{booktabs}
\usepackage{lineno}
\usepackage{enumitem}
\usepackage{paralist}
\usepackage{pdflscape}
\usepackage{setspace}
\usepackage[font=footnotesize,labelfont=bf]{caption}
\usepackage{colortbl}
\usepackage[table]{xcolor}
\usepackage{verbatim}
\usepackage{longtable}
\usepackage{tabu}
\usepackage{float}
\usepackage{titlesec}
\usepackage{lineno}
\usepackage{fancyhdr}
\usepackage{tocloft}% http://ctan.org/pkg/tocloft
\usepackage{multirow}
\usepackage{xspace}
\usepackage{hyperref}

\definecolor{lightgray}{HTML}{E8E8E8}
\definecolor{darkgray}{HTML}{DBDBDB}
\newcommand{\IE}{{\it i.e.,}\xspace}
\newcommand{\EG}{{\it e.g.,}\xspace}
\newcommand{\CF}{{\it c.f.,}\xspace}

\newcommand{\laser}{\texttt{laser}\xspace}
\newcommand{\treepar}{\texttt{TreePar}\xspace}
\newcommand{\expotree}{\texttt{expoTree}\xspace}
\newcommand{\gammastat}{$\gamma$-statistic\xspace}

\newcommand{\mrmadd}[1]{{\color{orange}#1}}


\bibliographystyle{sysbio.bst}

\setlength{\cftsecnumwidth}{2em}
\setlength{\cftsubsecnumwidth}{2.5em}
\setlength{\cftsubsubsecnumwidth}{3.2em}

\begin{document}

\title{\LARGE{Diversity-Dependent Diversification: Fact or Artifact?}}
\author{{\sc Andrew F. Magee$^{1\ast}$, Michael R. May$^1$, and Brian R. Moore$^1$} \\
\bigskip \\
\small$^1$\textit{Department of Evolution and Ecology, University of California, Davis} \\
\small\textit{Davis, CA 95616, USA} \\
\small{$^{\ast}$Corresponding author: afmagee@ucdavis.edu}
}
\date{}
\maketitle

%%%%%%%%%%%%
%     ABSTRACT      %
%%%%%%%%%%%%
\noindent\textit{Abstract.}---Since 2000, there has been great interest in using molecular phylogenies to gain insight into broad patterns of speciation and extinction across the Tree of Life. 
One surprising outcome of this work has been the apparent prevalence of diversification-rate decreases, estimated to occur in as much as 57\% of all phylogenies \citep{phillimore2008}. 
However, the prevalence of these decreases has tended to be based on studies using few of the many available methods to detect diversification-rate variation, and none have addressed the potential for either conflict among these methods, methodological biases. 
To assess the evidence for diversification-rate decreases, and better the understand forces that bias these estimates, we compiled 647 phylogenies from the primary literature. 
Overall, the prevalence of diversification-rate decreases could be as low as 3.8\% or as great as 71.3\%, but the inferred prevalence of diversification-rate decreases varies wildly based on several factors. 
Using a Bayesian logistic regression, we determine the order of importance of these factors to be whether the phylogeny was previously tested, how missing taxa are accounted for, the manner in which the phylogeny was made ultrametric, and the method used to test for diversification-rate decreases. 
We recommend several steps that studies of diversification-rate variation should take to ensure that their results are robust to these factors. 

\bigskip
\noindent Key words: diversity-dependent diversification; speciation; extinction; phylogeny; statistical bias.



%%%%%%%%%%%%
% INTRODUCTION %
%%%%%%%%%%%%
\clearpage
\noindent
Diversity-dependent diversification posits that the net-diversification rate (speciation -- extinction) of a lineage decreases through time owing to ecological interactions among its member species.
This hypothesis invokes an ecological carrying capacity for each lineage: the species in a growing lineage eventually exhaust available ecological opportunities (thereby decreasing the probability of speciation), and/or experience increasingly intense competitive ecological interactions (thereby increasing the probability of extinction).
The study of diversity-dependent diversification continues to be an exceptionally active locus of research \citep[\CF][]{Rabosky2013,moen2014}, including the development (and scrutiny) of statistical methods, the analysis of empirical data, and the elaboration of ecological and evolutionary theory.

Several statistical phylogenetic methods have been developed to detect patterns of temporal variation in diversification rates that may result from diversity-dependent diversification
\citep[\EG][]{Paradis1997, Paradis1998, Pybus2000, Rabosky2006, Morlon2010, Morlon2011, Morlon2015, Stadler2011, Etienne2012a, Etienne2012b, Leventhal2014, Hohna2016}. 
In general, these approaches involve evaluating the relative fit of the phylogenetic `observations' (the vector of branching times in the estimated tree) to alternative stochastic-branching models.
For example, we can compare the relative fit of a given tree to a model in which diversification rates are constant through time to an \textit{implicit} diversity-dependent diversification model (where diversification rates decrease through time for \textit{any} reason),
or to an \textit{explicit} diversity-dependent diversification model \citep[where diversification rates decrease as a function of the number of species;][]{Rabosky2006, Morlon2010, Morlon2011, Morlon2015, Etienne2012a, Leventhal2014}.  

Additional methodological research in this area has identified factors that may cause the prevalence of diversity-dependent diversification to be overestimated.
Spurious detection of diversity-dependent diversification may be caused both by \textit{phylogenetic} biases (associated with the procedure used to estimate study phylogenies) and also by \textit{methodological} biases (associated with the statistical approaches used to detect diversity-dependent diversification from study phylogenies).
Phylogenetic biases include: 
(1) discordance among gene trees in multi-locus datasets \citep{Stadler2016}; 
(2) the use of an underspecified substitution model \citep{Revell2005}, and; 
(3) the inclusion of an incomplete \citep{Pybus2000} and/or non-random \citep{Brock2011,Cusimano2012} sample of species in the phylogenetic analysis. 
Methodological biases for detecting diversification-rate variation include: 
(1) the use of the likelihood-ratio test to select among stochastic-branching models, which has been shown to be biased toward more complex (diversity-dependent) models \citep{Etienne2016}, and; 
(2) violation of the assumptions of these statistical methods, such as variation in diversification rates across lineages \citep{Pybus2000} and protracted speciation \citep{Chan1999,Etienne2012,Lambert2014}.
%and violation of the Markov property by protracted speciation \citep[][]{Chan1999,Etienne2012,Lambert2014a}.

Several studies have applied these statistical methods to large samples of empirical trees to assess the prevalence of diversity-dependent diversification: (1) \citet{McPeek2008} applied the \gammastat \citep{Pybus2000} to a sample of 240 trees; (2) similarly, Phillimore and Price (2008) applied the \gammastat to a sample of 45 trees, and; (3) \citet{Morlon2010} applied their coalescent-based statistical approach \citep{Morlon2011, Morlon2015} to the set of trees from both previous studies.
These empirical surveys have generally substantiated theoretical arguments \citep[\EG][]{Sepkoski1978, Levinton1979, Etienne2012a, Rabosky2013} predicting that diversity-dependent diversification should be a pervasive feature of empirical trees.

Accordingly, it is widely accepted that diversity-dependent diversification is either \textit{known} to be a pervasive feature of empirical phylogenies, or that it \textit{should} be empirically pervasive, even if the detection of diversity-dependent diversification is somewhat inflated by methodological artifacts.
Here, we explore the prevalence of diversity-dependent diversification by means of an empirical survey that greatly increases both the sampling intensity (comprising a sample of 391 trees) and methodological diversity (incorporating four statistical methods) of previous efforts.
Our survey reveals tremendous uncertainty regarding the empirical prevalence of diversity-dependent diversification (the detection rate in the sampled trees ranges from 4 to 71\%).
We explore the source of this uncertainty by performing a Bayesian logistic regression analysis that reveals the factors that must be overcome in order to understand the extent to which diversity-dependent diversification has shaped the Tree of Life.



%%%%%%%%%%%%%%%%%%
% MATERIALS AND METHODS %
%%%%%%%%%%%%%%%%%%
%\clearpage
\section*{Methods}


\subsection*{Data collection and curation}
We compiled two datasets for our survey.
We assembled a \textit{test} dataset comprising the $240$ trees from \citet{McPeek2008} and trees from all empirical applications of the two most popular methods for detecting temporal decreases in diversification rate; \IE the $\gamma$-statistic \citep{Pybus2000} and the birth-death likelihood approach \citep{Rabosky2006}.
We also assembled a \textit{control} dataset comprising all empirical phylogenetic studies published between 2003--2013 in \textit{Systematic Biology} for which alignments were deposited in TreeBASE \citep{Sanderson1994b}; the originating studies did not evaluate the control trees for temporal variation in diversification rates.

We then curated the resulting sample of 526 (490 test and 136 control) datasets as follows: 
(1) we ensured that internal nodes represented speciation events by pruning tips/sequences from all trees/alignments that included multiple accessions for a single described species; 
(2) we controlled for potential artifacts associated with polytomies \citep[\CF][]{Kuhn2011} by excluding trees with $\leq 60\%$ binary internal nodes ($77\%$ of the remaining trees were fully resolved), and;
(3) we ensured the independence of each tree by excluding replicate study trees for the same taxonomic group (favoring the retention of more fully sampled phylogenies).

The post-curation sample included 391 datasets (339 test trees and 52 control alignments).
We first estimated phylogenies for the control alignments using \texttt{GARLI} \citep{zwickl06}, and then estimated divergence times for these trees---and the subset of test trees from \citet{McPeek2008}, which were not ultrametic---using the nonparametric rate smoothing \citep[NPRS;][]{sanderson97} and the penalized-likelihood \citep[PL;][]{sanderson02} approaches implemented in \texttt{ape} \citep{ape}.
We provide details of the data-curation process in the Supporting Information (Figure \ref{fig:analysis_pipeline}).


\subsection*{Diversification-rate analyses}
We evaluated each study tree for evidence of decreasing diversification rates using four methods: 
(1) the $\gamma$-statistic \citep{Pybus2000} implemented in \texttt{ape} \citep{ape};
(2) the birth-death likelihood approach \citep{Rabosky2006} implemented in \texttt{laser} \citep{Rabosky2006b}; 
(3) the episodic birth-death approach \citep{Stadler2011} implemented in \texttt{TreePar} \citep{Stadler2015}, and
(4) the epidemiological model \citep{Leventhal2014} implemented in \texttt{expoTree} \citep{Leventhal2013}.
These methods differ in two key respects (Table \ref{tab:method_capabilities}): 
(1) the $\gamma$-statistic and \texttt{TreePar} implicitly model diversity-dependent diversification, whereas \texttt{laser} and \texttt{expoTree} explicitly model diversity-dependent diversification, and;
(2) \texttt{TreePar} and \texttt{expoTree} allow incomplete species sampling to be accommodated under the uniform model (which assumes that species are sampled randomly), whereas the $\gamma$-statistic and \texttt{laser} allow incomplete species sampling to be accommodated under both the uniform model and the diversified model \citep[which assumes that species are sampled to maximize taxonomic diversity;][]{hohna2011,Hohna2014a}.
We provide details of the diversification-rate analyses in the Supporting Information (see Section "Divergence-time estimation").


\subsection*{Logistic-regression analyses}
We explored the impact of several factors on the detection of temporal diversification-rate variation using Bayesian logistic regression.
Specifically, we explored the impact of five predictor variables:%
\begin{inparaenum}[(1)]
	\item the size of the study tree;
	\item the fraction of species included in the tree (the sampling fraction, $\rho$);
	\item the model used to describe incomplete species sampling (uniform or diversified);
	\item the method used to estimate divergence times (NPRS, PL, or BEAST), and;
	\item the method used to detect temporal diversification-rate variation ($\gamma$-statistic, \texttt{laser}, \texttt{TreePar}, or \texttt{expoTree}).
\end{inparaenum}%

The logistic-regression model assumes that the result of each diversification-rate analysis is a Bernoulli trial, where the probability of `success'---in this case, successfully detecting a significant decrease in diversification rate---is determined by the predictor variables associated with that trial.
We estimated the posterior distributions of the effect size (regression coefficient), $\beta$, for each predictor variable using the \texttt{R} package \texttt{BayesLogit} \citep{Polson2013}.
Positive values of $\beta$ indicate that a predictor variable increases the probability of detecting temporal decreases in diversification rate.
Because the test and control datasets exhibited strikingly different detection patterns (Figure \ref{fig:pie_charts}), we fit separate logistic-regression models to each dataset.
We performed posterior-predictive testing to verify that our logistic-regression models provided a good description of the factors influencing the detection of temporal decreases in diversification rate.
%We assessed the absolute fit of the logistic-regression models to each dataset by performing posterior-predictive testing to verify that our logistic-regression models provided a good description of the factors influencing the detection of temporal decreases in diversification rate.
We provide a comprehensive description of our logistic regression models, MCMC diagnosis, and posterior-predictive testing in the Supplemental Material (See section "Logistic regression analyses," Figures \ref{fig:test_pps} and \ref{fig:control_pps}).



%%%%%%%%%%%%%%%%%%%
%  RESULTS AND DISCUSSION  %
%%%%%%%%%%%%%%%%%%%
\section*{Results and Discussion}
We compiled the most comprehensive molecular phylogenetic dataset to date---comprising 391 unique phylogenies, ranging in size from 4 to 796 species and in sampling fraction from 0.0015 to 1 (see Figures \ref{fig:rho_hist} and \ref{fig:tree_size_hist})---revealing that the prevalence of diversity-dependent diversification is subject to extreme uncertainty: depending on the source of the phylogeny and the types of analyses used, the prevalence of diversity-dependent diversification ranges between $3.8\%$ to $71.3\%$ (Table \ref{tab:results_summary}, Figure \ref{fig:pie_charts}).
The impact of the factors contributing to this uncertainty are, in descending order of importance:%
\begin{inparaenum}[(1)]
	\item data provenance (in particular, whether the data had previously been analyzed for density-dependent diversification);
	\item the method used to estimate divergence times (\IE the data used to fit diversification models);
	\item the assumed model of incomplete taxon sampling (\IE whether or not taxa are missing randomly from the tree), and;
	\item the method used to test for diversity-dependent diversification.
\end{inparaenum}

\begin{table}[H]
\small
\caption{\bf{Fraction of phylogenies displaying diversification-rate decreases in each dataset.}}
\label{tab:results_summary}
\centering
\begin{tabular} {llcccccc}
\toprule
& & \multicolumn{6}{c}{Method} \\
\cline{3-8}
Dataset 		& Divergence-time 	& \multicolumn{2}{c}{\texttt{$\gamma$-statistic}} 	& \multicolumn{2}{c}{\texttt{laser}} 		& \texttt{TreePar} 	& \texttt{expoTree} 	\\
			& estimator 		& (uniform)						& (diversified)					& (uniform)			& (diversified)			& (uniform)			& (uniform)				\\
\midrule
\rowcolor{lightgray}             & NPRS  & 41.0\% & 20.5\% & 37.8\% & 18.1\% & 26.7\% & 33.7\% \\
\rowcolor{lightgray} \emph{test} & PL    & 69.8\% & 52.3\% & 71.3\% & 50.2\% & 57.4\% & 70.7\% \\
\rowcolor{lightgray}             & BEAST & 34.7\% & 11.2\% & 37.1\% & 13.4\% & 16.5\% & 50.6\% \\
\rowcolor{darkgray}			& NPRS 				& 13.5\% 						& 3.8\% 						& 13.7\% 			& 3.9\% 				& 19.2\% 			& 22.2\% 				\\
\rowcolor{darkgray}\multirow{-2}{*}{\emph{control}}			& PL 				& 17.3\% 						& 7.7\% 						& 20.9\% 			& 9.3\% 				& 19.2\% 			& 21.1\% \\
\bottomrule
\end{tabular}
\end{table}

\begin{figure}[H]
	\centering
	\includegraphics[width=1.0\linewidth]{./figures/pie_charts.pdf}
	\caption{
			Inferred prevalence of diversification-rate decreases for each analysis method (columns) and divergence-time estimator (rows).
			The two shades of orange represent \emph{implicit} (light orange) and \emph{explicit} (dark orange) models of diversity dependence.
			Blue is represents the portion that is blue is the fraction of phylogenies \emph{not} inferred to have experienced a decrease.
			Specifically, blue includes failure to reject constant rates (\gammastat), preference for a constant-rate model (\laser, \treepar, and \expotree), or preference for a model where rates increase (\laser and \treepar).
			Each pie chart is includes only successful tests accomplished with a given method, whereas Figures \ref{fig:heatmap_test_nprs}, \ref{fig:heatmap_combined} also display the phylogenies for which the methods failed.
}
	\label{fig:pie_charts}
\end{figure}

Data provenance is the strongest predictor of the prevalence of diversification-rate decreases: on average, previously tested phylogenies showed a much greater prevalence of diversification-rate decreases ($43.0\%$) than untested phylogenies ($13.9\%$) (Table \ref{tab:results_summary}, Figure \ref{fig:pie_charts}). 
Evidently, no good estimate of the empirical prevalence of diversity-dependent diversification can result from merely surveying published results. 
A good estimate would require sampling clades randomly (without replacement) from the Tree of Life, including groups that have not been previously tested for diversification-rate decreases.

The method used to estimate divergence times also strongly influenced the prevalence of diversity-dependent diversification; in the most extreme case, diversity-dependence ranged from $33.7\%$ (using non-parameteric rate smoothing) to $70.7\%$ (penalized likelihood) (Table \ref{tab:results_summary}, Figure \ref{fig:pie_charts}.
The estimated regression coefficient for penalized likelihood, relative to non-parametric rate smoothing, was 1.62 (95\% CI [1.44 to 1.81]); consequently, divergence times estimated using penalized likelihood were $35.8\%$ more likely to demonstrate evidence for diversity-dependent diversification (Figure \ref{fig:logistic_model_coefficients}).
This result is hardly surprising: as divergence-time estimates are the raw data to which diversification models are fit, the idiosyncrasies of individual divergence-time estimators may have profound consequences for the detection of diversity-dependent diversification.
This emphasizes the importance of throughly evaluating the performance of the various divergence-time estimators for individual datasets, \EG using robust parametric model selection procedures \citep{baele12}.

Consistent with previous studies \citep{Brock2011,hohna2011,Cusimano2012}, the model used to accommodate incomplete taxon sampling also had a significant impact on the prevalence of diversity-dependent diversification.
The regression coefficient for diversified sampling, relative to uniform sampling, was -1.05 (95\% CI [-1.25 to -0.85]), \IE assuming diversified taxon sampling decreased the probability of detecting diversity dependence by 25.6\% (Figure \ref{fig:logistic_model_coefficients}).
Both uniform and diversified taxon sampling represent extreme endpoints of a continuum---from completely random to completely non-random---whereas real taxon sampling is likely somewhere in between.
Ideally, an approach will be developed that relaxes the extreme assumptions of these taxon sampling models.
In the interim, we suggest that studies of diversification rates assess the fit of different taxon-sampling schemes to their data \citep{hohna2011}, though these problems can clearly be avoided by employing complete species sampling. 

\begin{figure}[H]
	\centering
	\includegraphics[width=0.33\linewidth]{./figures/logistic_coefficients_simplified_beanplot.pdf}
	\caption{Regression coefficients of the logistic regression analysis.
			 %Each boxplot represents the marginal posterior distribution (spanning the 95\% credible interval) for the effect size of various predictors estimated using Bayesian logistic regression.
			 Pictured are the posterior probability densities for the effect size of various predictors estimated using Bayesian logistic regression, with horizontal bars showing the 95\% credible intervals.
			 Positive values of the effect size indicate that the predictor increases the probability of inferring diversification-rate decreases, while a negative effect size indicates a decreased probability.}
	\label{fig:logistic_model_coefficients}
\end{figure}

The weakest predictor of density-dependent diversification was the actual method used to detect it, which causes the prevalence to vary by only $\approx15\%$.
The estimated regression coefficients of the \gammastat ($\beta = 0.729$, 95\% CI [0.475 to 0.984]), \laser ($\beta = 0.687$, 95\% CI [0.430 to 0.946]), and \expotree ($\beta = 0.695$, 95\% CI [0.423 to 0.968]) reflected nearly identical average detection rates of diversity-dependent diversification, all slightly greater than \treepar (15.3\%, 14.5\%, and 14.6\% increases respectively).
However, for any particular phylogeny, each method potentially provided a very different answer (Figures \ref{fig:heatmap_test_nprs}, \ref{fig:heatmap_combined}): on average, any two methods provided qualitatively different results on 17.9\% of our analyzed trees, and for 3.1\% of the trees, at least two methods provided qualitatively \emph{contradictory} inferences (\IE one method inferred a diversification-rate increase, and another inferred a diversification-rate decrease).
We therefore urge caution in interpreting the results of a single analysis, and recommend that users compare the performance of many available methods \citep[differences in likelihood functions notwithstanding, \CF][]{stadler2013}.
We join others in the call for further method development, including more comprehensive characterization of the behavior of these methods, and, ideally, a unified framework for assessing the dynamics of lineage diversification \citep{Morlon2014,Etienne2016}.

\begin{figure}[h!]
\begin{center}
\includegraphics[]{./figures/heatmap_test_nprs.pdf}
\end{center}
\caption{Heat map of the results for each analysis method (excluding diversified sampling) on every NPRS \emph{test} phylogeny. The rows are the phylogenies, the columns are, from left to right, the \gammastat, the \gammstat employing the diversified model of taxon sampling, birth-death likelihood, birth-death likelihood employing the diversified model of taxon sampling, the episodic birth-death approach, and the epidemiological diversity-dependent approach. A cell is orange if that method detected a decrease in diversification on that phylogeny (diversity-dependent or otherwise), green if it detected an increase, blue if it failed to reject constant rates, and grey if the analysis failed.}
\label{fig:heatmap_test_nprs}
\end{figure}

%what are the implications our results for empirical analyses and for methods developers
% implications for empiricists:
% (1) rigorously explore the fit of competing models used to estimate divergence times \citep[][]{Bael2012, Heath2014}; 
% (2) assess the sensitivity of results to alternative models of species sampling;
% (3) assess the robustness of conclusions to alternative methods for detecting temporal diversification-rate variation, and;
% (4) efforts to characterize the general empirical prevalence of diversity-dependent diversification should sample trees randomly (rather than focussing on previously analyzed datasets).
%
% priorities for method developers:
% (1) develop more realistic models of species sampling, and; 
% (2) continue recent efforts \citep[{\it e.g.},][]{} to better characterize the statistical behavior of methods for detecting temporal diversification-rate variation via simulation.
%
In general, our results indicate that empirical evidence for diversity-dependent diversification is equivocal.
Our logistic regression analysis identifies the challenges researchers need to overcome in order to clarify the role of diversity-dependent diversification in shaping molecular phylogenies, namely:%
\begin{inparaenum}[(1)]%
	\item take care to select phylogenies that represent a random sample of the Tree of Life;
	\item rigorously compare the fit of various models---including relaxed-molecular clocks for divergence time estimation, taxon-sampling schemes, and lineage-diversification models---to empirical data, to ensure that the biological conclusions regarding diversity-dependence are as robust as possible to modeling assumptions, and;
	\item develop more realistic models of lineage diversification and taxon sampling in order to reduce the impact of methodological artifacts.
\end{inparaenum}%



%%%%%%%%%%%
%    SUMMARY     %
%%%%%%%%%%%
\section*{Conclusions}
The intense research focus on diversity-dependent diversification suggests that it perceived to be an important biological phenomenon.
Our study---based on (re)analyses of the largest sample of empirical datasets using  the greatest diversity of statistical methods---reveals tremendous uncertainty regarding the empirical prevalence of diversity-dependent diversification.
On one hand, the upper range of our estimates ($\sim 71\%$) is consistent with the perspective that diversity-dependent diversification is a pervasive feature of empirical phylogenies, and would substantiate arguments that this phenomenon should be accommodated in all models used to study lineage diversification \citep[{\it e.g.},][]{Rabosky2014}, and perhaps also in phylogenetic models used to estimate divergence times. 
By contrast, the lower range of our estimates ($\sim 4\%$) implies that diversity-dependent diversification may be a rare phenomenon in nature.
Moreover, our study provides empirical evidence substantiating theoretical concerns (refs) and simulation studies (refs) that have proposed possible factors that may impact our ability to identify diversity-dependent diversification. 
Results of our Bayesian logistic regression analyses reveal the relative importance of those factors in real datasets
 	-of previously identified factors (species-sampling methods, diversification-rate methods)
 	-also reveal new factors (divergence-time estimator)
Understanding the impact of these factors is important both to empirical biologists (as it provides guidance to on the application of these methods) and to theoretical biologists (as it provides guidance on the priorities for developing methods to resolve these issues).

\paragraph*{Data Availability}
The authors confirm that all of the empirical datasets---and the \texttt{R} scripts used to analyze and summarize those data---are available from the Dryad Digital Repository: XXXXX



%%%%%%%%%%%%%%%%
% ACKNOWLEDGEMENTS  %
%%%%%%%%%%%%%%%%
\vspace{-3mm}
\section*{Acknowledgments}
We are grateful to Gabriel Leventhal for advice regarding \texttt{expoTree} and to Sebastian H{\"o}hna for insightful discussion. 
This research was supported by National Science Foundation (NSF) grants DEB-0842181, DEB-0919529, DBI-1356737, and DEB-1457835 awarded to BRM.
Computational resources for this work were provided by NSF XSEDE grants DEB-120031, TG-DEB140025, and TG-BIO140014 awarded to BRM.



%%%%%%%%%%%
% REFERENCES  %
%%%%%%%%%%%
\clearpage
\bibliography{bayes.bib}

\clearpage
\include{prevalence_SI}

\end{document}






