%\nolinenumbers
\section*{\centering Supplementary Material}
\setcounter{page}{1}
\setcounter{section}{0}
\setcounter{table}{0}
\setcounter{figure}{0}
\setcounter{equation}{0}
\renewcommand{\thesection}{SI.\arabic{section}}
\renewcommand{\theequation}{S.\arabic{equation}}
\renewcommand{\thefigure}{S.\arabic{figure}}
\renewcommand{\thetable}{S.\arabic{table}}
\renewcommand{\thepage}{S\arabic{page}}

\subsection*{Methods}

\subsubsection*{Data source}
The phylogenies analyzed in these studies come from three distinct sources: two published papers \citep{McPeek2008,Magee2014} and a novel set obtained specifically for this study as a control.
Potential phylogenies for the control dataset were selected from studies published in Systematic Biology between 2003 and 2013.
Alignments were downloaded from TreeBASE \citep{Sanderson1994b} when available, and inspected to determine whether there was sufficient information present to partition the alignment in the paper of origin and the nexus alignment. If a study specified that an alignment had been partitioned and we could not obtain enough information to recreate this partitioning scheme, we discarded it. 
If the study from which the phylogeny originated conducted an unpartitioned analysis, we used jModelTest 2.1.7 \citep{darriba2012} to determine the best fitting model of substitution for the entire alignment. 
For those phylogenies that had originally been partitioned, we used PartitionFinder 1.1.1 \citep{lanfear2012} to determine the best partitioning scheme, using the greedy algorithm, setting the model selection criterion to BIC, and setting the original study's partitions as the data subsets.
We specified that PartitionFinder should consider all available DNA substitution models except combinations with both gamma-distributed rate variation and a proportion of invariant sites, due to generally weak identifiability of this mixture model. 
For consistency, we post-processed the output from jModelTest and removed from consideration all models that we did not specify in PartitionFinder.
Phylogenies were then inferred using GARLI 2.01 \citep{zwickl06}, specifying the best-fit substitution models and partitioning schemes, 5 separate searches (searchreps=5), and otherwise default settings. 

\subsubsection*{Dataset curation}
All phylogenies were inspected to ensure that they were species-level, taxonomically distinct from other studies, and did not contain an excessive fraction of unresolved internal nodes (polytomies). 
To assess whether phylogenies were at the species level, the names of species in each phylogeny were checked for uniqueness using a combination of \verb!R! scripting and manual investigation. 
When a phylogeny appeared to have multiple representatives of the same species, the original study was consulted. 
Unless there was convincing evidence that a study believed multiple representatives of a species should be included in that phylogeny, tips were pruned until only one tip per species remained. 
Convincing evidence meant either a statement in the study (\EG acknowledging that the species was found not to be monophyletic or suggesting that the species was likely a cryptic species complex) or a figure showing a resolved node separating the representatives of the species (\textit{i.e.} the tips were not descended from a polytomy). 
The taxonomic uniqueness of phylogenies was checked first by considering the names of the focal clades for the phylogenies. 
Then, the names of the genera included in all phylogenies were extracted and compared against each other using custom \verb!R! scripts.
A list of phylogenies that contained shared genus names was generated, and those phylogenies were checked manually. 
In a case where two phylogenies were found to overlap taxonomically---whether they included the same species, genus, or higher taxon---the less fully sampled phylogeny was removed from the dataset. 
Phylogenies were inspected manually for polytomies, and any phylogeny containing a large proportion of unresolved internal nodes was removed. 

To guarantee that the sampling fraction (the number of species in the phylogeny divided by the number of extant species descended from the MRCA) for all phylogenies was calculated consistently, during the curation process, the true number of species in each clade was recorded. 
For consistency with the original studies, if there was a stated estimate of this diversity, that number was used. 
In cases where this number could not be obtained directly from a study, we consulted Wikipedia for information, and if that failed we performed a Google search. 
If necessary, the true size of the clade was estimated by summing over the diversities of clades found to be included in the phylogeny. 
When multiple representatives of a species were included in a study, or if a new species was suggested, the true clade size was adjusted accordingly. 

\subsubsection*{Divergence-time estimation and categorization}
There are a number of available methods that can be employed to estimate the divergence times of a phylogram without the use of sequence data. 
%In his 2008 study \citep{mcpeek2008}, McPeek used Mean Path Length dating \citep{britton2002} to estimate divergence times, having gathered the phylogenies from printed copies using calipers and hand-writing newick strings. 
%When we used MPL the non-ultrametric trees obtained from McPeek, we found that MPL generated surprising number of phylogenies with negative branch lengths, which are biologically impossible. 
%Both the Yule and the birth-death processes assume that time moves in a single direction, and since every method that can be used to test a tree depends on one of these models, we chose not to employ MPL on any of our phylogenies.
To assess the effect of different divergence time estimators on the prevalence of diversification-rate decreases, we employed both non-parametric rate smoothing (NPRS) \citep{sanderson97} and penalized likelihood (PL) \citep{sanderson02} to make McPeek's phylogenies ultrametric.
For consistency, we also used these methods on the \emph{control} phylogenies.
Penalized likelihood was employed using the \texttt{chronopl} function implemented in the \verb!R! package \verb!ape!, employing cross validation and setting lambda (the so-called smoothing parameter) to 1. 
NPRS is no longer a feature included in current releases of \verb!ape!, so to implement it we downloaded an archived version, 2.3-1, from CRAN, obtained all relevant \verb!C! and \verb!R! code and compiled the function locally. 
The phylogenies obtained from \citet{Magee2014} were already ultrametric, so they were categorized by how the original studies estimated divergence times. 
In cases where it was unclear how a study estimated divergence times, the phylogeny was discarded.
To ensure that there was enough information to estimate logistic regression parameters for the divergence time estimators, we only included methods that were applied to at least five phylogenies, which were BEAST, NPRS, and PL.

\subsubsection*{Diversification-through time analyses}
There are a number of methods that have been described for testing whether a phylogeny has experienced temporal variation in diversification rates, most of of which are implemented in \texttt{R} packages. 
The most popular of these is the $\gamma$-statistic \citep{Pybus2000}, implemented in numerous \texttt{R} packages, including \verb!ape! \citep{ape}. 
Also popular is Rabosky's birth-death likelihood method \citep{Rabosky2006}, implemented in the package \verb!laser! \citep{Rabosky2006b}. 
Less widely used methods include Paradis' Survival Analysis \citep{Paradis1997}, implemented in  \verb!ape!, Etienne's diversity-dependent diversification likelihood method \citep{Etienne2012a}, implemented in the package \verb!DDD!, Stadler's birth-death with shifts method \citep{Stadler2011}, implemented in \verb!TreePar!, and Leventhal's epidemiological diversity-dependent likelihood method \citep{Leventhal2014}, implemented in the package \verb!expoTree!.
Survival Analysis assumes that phylogenies diversified under a pure-birth process, potentially leading to spurious detection of variation in diversification rate variation \citep{Pybus2000}.
Further, while Survival Analysis employs three models, only one is not also included in \laser, and Survival Analysis lacks a technique for directly selecting the best of the candidate models, so we did not apply it to our phylogenies.
While Etienne's package \verb!DDD! allows for an impressive flexibility in both models of diversity-dependence and time-dependence, the length of its run time precluded our use in this analysis. 

The $\gamma$-statistic is a normalized weighted average of the branching times in a phylogeny.
If the branching times in a phylogeny fall closer to the root than expected under a pure-birth (Yule) process, the $\gamma$-statistic is negative.
% Fortunately, extinction does not affect the performance of the $\gamma$-statistic.
If the $\gamma$-statistic is significantly negative (under the standard normal), the null hypothesis of a constant rate of diversification is rejected.
A significantly negative $\gamma$ value does not imply what form the decrease takes; that is, it cannot discriminate between a time-dependent model and a diversity-dependent model as birth-death likelihood can.
If a phylogeny has incomplete taxon sampling, the $\gamma$-statistic will be biased towards detecting decreases \citep{Pybus2000}. 
Therefore, in the presence of incomplete species sampling, Monte Carlo simulation must be employed to compute the correct null distribution for the test statistic.
For each phylogeny we analyzed, we simulated 10,000 pure-birth phylogenies with the same size and sampling fraction as the empirical phylogeny, calculated the gamma statistic for each, and used this null distribution to assess the significance of the $\gamma$ values.
When simulating the null distribution, any model of taxon sampling can be used to account for incomplete sampling, but historically sampling has always been assumed to be random.
Nonrandom sampling can cause a severe negative bias in the value of $\gamma$ that is not captured by the random sampling model, leading to inflated Type 1 error.
To address this issue, we also simulated phylogenies using H{\"o}hna et al.'s diversified model \citep{hohna2011}, which assumes that the taxa present in a phylogeny represent the oldest possible divergences for a given sampling fraction.
While obviously an oversimplification of reality, this model allows us to set a lower limit on the prevalence of diversity-dependence, while the uniform model sets the upper limit.

Rabosky's birth-death likelihood approach \citep[\laser,][]{Rabosky2006} allows a user to consider a range of time-dependent and diversity-dependent models of diversification. 
To perform model selection, \verb!laser! uses a statistic based on the AIC \citep{akaike1974}, the $\Delta$AIC$_{RC}$, calculated by subtracting the AIC score from the best fitting variable rate model from the AIC score of the best fitting constant rate model. 
If the $\Delta$AIC$_{RC}$ is positive, we reject constant-rate models in favor of variable-rate models and take the variable-rate model with the best AIC score to be the best model of diversification for the observed phylogeny. 
Following convention, we implemented two constant-rate models, the Yule (pure birth) and birth-death, and three variable-rate models, the Yule-two-rate (which divides the tree into two time windows and fits a different Yule model to each), DDL (where the effect of diversity-dependence is logistic), and DDX (where the effect of diversity-dependence is exponential). 
It is also possible to use the $\Delta$AIC$_{RC}$ as a test-statistic and simulate a null distribution (that is, to perform Monte Carlo simulation), which allows us to account for incomplete taxon sampling using both the uniform random and diversified models of taxon sampling as with the $\gamma$-statistic. 

The episodic birth-death likelihood approach \citep[\treepar,][]{Stadler2011} divides a phylogeny into some number of windows in time, each of which is governed by a different set of birth-death parameters. 
The user specifies the maximum number of rate shifts, \emph{n}.
\treepar then discretizes the potential locations for rate shifts on the phylogeny and evaluates all possible placements of rate shifts. 
\treepar provides the likelihoods (and estimated parameters) for models with 0 (constant diversification) up to \emph{n} rate shifts.
To determine the number of rate shifts that best describes the phylogeny, Hierarchical Likelihood Ratio tests are employed, starting with a constant rate tree and continuing up to the maximum number of breakpoints considered.
To allow for a simple classification of a tree as either displaying a decrease or not, we chose \emph{n} = 1, such that we compared a constant rate model to a two-rate model. 
We specified 10 possible shift locations per phylogeny, distributed uniformly in time.
As implemented in \verb!TreePar!, the episodic birth-death likelihood approach accommodates missing taxa under the assumption that they were randomly sampled (\IE it does not accommodate non-random taxon sampling).

The epidemiological diversity-dependent likelihood approach derives the likelihood of a phylogeny under a stochastic Susceptible-Infected-Susceptible model, with or without a diversity-dependence parameter.
The method can be applied to reconstructed species-level phylogenies when the rate of serial sampling is set to 0 (which in this context indicates the rate at which fossil species are recovered from the rock record), and by treating transmission events as speciation events and death/recovery events as extinctions.
We chose not to use the inbuilt functions \verb!expoTree.optim!  to fit the diversity-dependent and constant models, which are designed to work better with epidemiological than species phylogenies (Leventhal, personal communication).
We followed the original implementation in calling \verb!optim()! using the \verb!Nelder-Mead! optimization method, fixing the sampling fraction, $\rho$ to the observed value, and fixing $\psi$---the epidemiological parameter for sequential sampling---to 0.
As implemented in \verb!expoTree!, the epidemiological diversity-dependent likelihood approach accommodates missing taxa under the assumption that they were randomly sampled.

Each of the four methods we implemented have both strengths and weaknesses, summarized in Table \ref{tab:method_summary}. 
Due to the use of Monte Carlo simulation, the $\gamma$-statistic and birth-death likelihood have the advantage of being able to incorporate different models of taxon sampling.
The $\gamma$-statistic also has the advantage of being quite well characterized for biases \citep[\CF][]{Revell2005,Liow2010,Brock2011,Cusimano2012,pennell2012}, but the lack of explicit model renders it incapable of discriminating between diversity-dependent and prosaic in diversification-rate variation.
The only models implemented in \laser of diversity-dependence are strictly pure-birth, and as conventionally implemented, it includes no variable-rate model with extinction.
\treepar has the advantage of explicitly and directly addressing both missing species and extinction, though it cannot detect diversity-dependence directly, so it serves to provide a more generous estimate of the maximum prevalence of diversity-dependence.
The only method we used that accounts for extinction and diversity-dependence concurrently is \expotree, though in having no model of time-dependent diversification, it is likely identifying diversity-dependence in cases where the true best model is time-dependent. 
Recent work \citep{Etienne2016} suggests that \expotree, by merit of using likelihood-ratio tests to compare a constant rate model to a model of diversity-dependence, suffers from inflated Type 1 error.
It is possible that this is a more general phenomenon and therefore also affects \treepar.
This suggests that the upper limit on the prevalence of diversity-dependence established by both of these methods are perhaps very generous, but does not undermine the uncertainty surrounding the prevalence of diversity-dependence.

\begin{table}[H]
\caption{\bf{Characteristics of methods used to detect temporal diversification-rate variation.}}\label{tab:method_summary}
\label{tab:method_capabilities}
\centering
\begin{tabular} {lcccc}
\toprule
 	   					& explicit diversity-dependent 	& time-dependent 	& \multicolumn{2}{c}{species-sampling model}\\
method 					& model 				  	& model	 		& random			&  diversified			\\
\midrule
\texttt{$\gamma$-statistic} 	& no 					& yes 			& yes 			& yes				\\
\rowcolor{gray!25}
\texttt{laser} 				& yes 					& yes  			& yes			& yes				\\
\texttt{TreePar} 			& no 					& yes 			& yes 			& no					\\
\rowcolor{gray!25}
\texttt{expoTree} 			& yes 					& no 			& yes 			& no					\\
\bottomrule
\end{tabular}
\end{table}


\subsubsection*{Logistic regression analyses}
We follow the logistic regression methods in \citep{Magee2014} to understand the relationship between various biological and methodological features and the detection of diversification-rate variation through time; for completeness, we present complete details of this approach here.
We used Bayesian logistic regression to explore correlations between the detection of diversification-rate decrease and several variables. 
Under this approach, a \emph{trial} is a diversification-through time analysis of a given phylogeny---made ultrametric with a particular method---using a particular diversification-rate through time analysis method, which we deem a \emph{success} if that phylogeny displays a diversification-rate decrease. 
The outcomes of a set of $n$ trials are contained in a data vector $\boldsymbol{x} = \{x_1,x_2,\ldots,x_n\}$, where $x_i$ is 1 if phylogeny $i$ displays a diversification-rate decrease and is 0 otherwise. 
The outcome of each trial depends on a set of $k$ \emph{predictor variables} that may be numerical (\emph{e.g.}, the size of the phylogeny) or categorical (\emph{e.g.}, whether taxon sampling is assumed to be uniform or diversified). 
An $n \times k$ matrix $\mathcal{I}$, the \emph{design matrix}, describes the relationships between trials and predictor variables: $\mathcal{I}_{ij}$ is the value for predictor variable $j$ for trial $i$. 
\emph{Parameters} relate the values of each predictor variable to the probability of success of each trial, and are described by the parameter vector $\boldsymbol\beta = \{\beta_1,\beta_2,\ldots,\beta_k\}$, where $\beta_i$ is the contribution of parameter $i$ to the probability of success.

In a Bayesian framework, we are interested in estimating the joint posterior probability distribution of the model parameters $\boldsymbol\beta$ conditional on the data $\boldsymbol{x}$. 
According to Bayes' theorem,
\begin{align*}
P(\boldsymbol\beta \mid \boldsymbol x) = \frac{ P( \boldsymbol{x} \mid \boldsymbol\beta  ) P(\boldsymbol\beta) }{ \int P( \boldsymbol{x} \mid \boldsymbol\beta  ) P(\boldsymbol\beta)\ \mathrm{d}\boldsymbol\beta },
\end{align*}
the \emph{posterior probability} of the model parameters, $P(\boldsymbol\beta \mid \boldsymbol x)$, is equal to \emph{likelihood} of the data given the model parameters, $P(\boldsymbol{x} \mid \boldsymbol\beta)$, multiplied by the \emph{prior probability} of the parameters, $P(\boldsymbol\beta)$, divided by the \emph{marginal likelihood} of the data.

Given the design matrix $\mathcal{I}$, the outcomes of each of the $n$ trials are conditionally independent, so that the likelihood of $\boldsymbol x$ is the product of the likelihoods for each individual trial:
\begin{align*}
P(\boldsymbol{x} \mid \boldsymbol\beta) = \prod_{i=1}^n P(x_i \mid \mathcal{I}, \boldsymbol\beta).
\end{align*}
The likelihood of observing the outcome of a particular trial is
\begin{align}\label{eqn:trial_likelihood}
P(x_i \mid \mathcal{I}, \boldsymbol\beta) = \begin{cases}
									\frac{1}{1+e^{-\omega_i} } & \text{if } x_i=1\\
									1-\frac{1}{1+e^{-\omega_i}} & \text{if } x_i=0,
								  \end{cases}
\end{align}
where
\begin{align}\label{eqn:trial_omega}
\omega_i = \sum_{j=1}^{k} \mathcal{I}_{ij}\beta_j.
\end{align}

We specified a multivariate normal prior probability distribution on the $\boldsymbol\beta$ parameters with means $\boldsymbol\mu$ and covariance matrix $\Sigma$. 
The complexity of the marginal likelihood precludes an analytical solution to the posterior probability distribution. 
Accordingly, we approximated the posterior probability distribution using the Markov chain Monte Carlo algorithm implemented in  the  \verb!R!  package \verb!BayesLogit! \citep{Polson2013}. 
This program uses conjugate prior and posterior probability distributions (via Polya-gamma-distributed latent variables), which permits use of an efficient Gibbs sampling algorithm to approximate the joint posterior distribution of $\boldsymbol\beta$ conditional on the data. 

We defined a set of predictor variables based on factors known to affect diversification-rate analyses.
We included an \emph{intercept} predictor variable to describe the background probability of detecting a decrease.
We treated \emph{tree size} and \emph{sampling fraction} as (mean-centered) continuous predictor variables, and \emph{diversification-rate analysis method}, \emph{sampling model}, and \emph{divergence time estimator} as discrete predictor variables.

Discrete predictor variables for logistic regression are generally binary, assuming values of 0 or 1. 
Aside from the model of taxon sampling, our discrete variables had more than two possible categories. 
We therefore adopted an \emph{indicator-variable} approach in which predictor variables with $p$ categories are discretized into $p$ distinct indicators; each phylogeny in a particular predictor category was then assigned a 1 for the corresponding indicator variable. 
Under this approach, phylogenies analyzed with the $\gamma$-statistic were assigned a 1 for the \emph{$\gamma$-statistic} variable, phylogenies analyzed with laser were assigned a 1 for the \emph{laser} variable, and phylogenies analyzed with expotree were assigned a 1 for the \emph{$\gamma$-statistic} variable. 
For the analysis of the control dataset, phylogenies made ultrametric with PL were assigned a 1 for the \emph{PL} variable. 
For the analysis of the ``test'' dataset, a unique indicator was assigned to each divergence time estimator in both the Magee et al. 
phylogenies and the McPeek trees to account for between dataset-heterogeneity in the inferred prevalence of diversification-rate decreases. 

Phylogenies made ultrametric with BEAST in the Magee dataset were assigned a 1 for the \emph{BEAST$_\text{Magee}$} variable, phylogenies made ultrametric with PL in the Magee dataset were assigned a 1 for the \emph{PL$_\text{Magee}$} variable, phylogenies made ultrametric with NPRS in the Magee dataset were assigned a 1 for the \emph{NPRS$_\text{Magee}$} variable, and phylogenies made ultrametric with PL in the McPeek dataset were assigned a 1 for the \emph{PL$_\text{McPeek}$} variable.
In order to avoid overparameteriziation of the logistic model, we did not assign indicator variables for the \emph{TreePar} variable in either analysis, the \emph{NPRS} variable in the control analysis, and the \emph{NPRS$_\text{McPeek}$} variable in the ``test'' analysis. 
Accordingly, the values for  $\gamma$-\emph{statistic}, \emph{laser}, and \emph{expotree} parameters are interpreted as effects relative to TreePar; similarly, the value for \emph{PL} is interpreted as the effect relative to NPRS in the control dataset, and \emph{BEAST$_\text{Magee}$}, \emph{PL$_\text{Magee}$}, \emph{NPRS$_\text{Magee}$}, and \emph{PL$_\text{McPeek}$} variables are interpreted as the effect relative to NPRS in the McPeek dataset for the \emph{test} analysis. 

We estimated parameters for each data subset by performing four independent MCMC simulations, running each chain for $2.5 \times 10^5$ cycles plus $10^3$ cycles as burnin. 
We assessed the performance of all MCMC simulations using the \texttt{Tracer} \citep[][]{Drummond2012} and \texttt{coda} \citep{Plummer2006} packages.
We monitored convergence of each chain to the stationary distribution by plotting the time series and calculating the Geweke diagnostic \citep[$p$,][]{Geweke1992} for every parameter.
We assessed the mixing of each chain over the stationary distribution by calculating both the potential scale reduction factor \citep[\emph{PSRF},][]{Gelman1992a} diagnostic and the effective sample size \citep[\emph{ESS}][]{Brooks1998b} for all parameters.
Values of all diagnostics for all parameters in all MCMC simulations indicate reliable approximation of the stationary (joint posterior probability) distributions: \EG $\text{\emph{ESS}} > 60000$; $\text{\emph{PSRF}} = 1$; $p > 0.05$ for each parameter.
Additionally, we assessed convergence by comparing the four independent estimates of the marginal posterior probability density for each parameter, ensuring that all parameter estimates were effectively identical and SAE compliant \citep{Brooks1998b}.
Based on these diagnostic analyses, we based parameter estimates on the combined stationary samples from each of the four independent chains ($N = 10^6$).

To assess the adequacy of our logistic regression model, we performed posterior predictive simulations for both the \emph{test} and \emph{control} analyses. 
Each simulation consisted of a randomly drawing a vector of parameters, $\boldsymbol\beta_\text{sim}$, from the approximate joint posterior distribution generated by our logistic regression analysis.
For each random draw from the joint posterior distribution, we simulated a new posterior predictive dataset, where each element of the dataset was a success or failure according to $\boldsymbol\beta_\text{sim}$ and $\mathcal{I}$, using equations \ref{eqn:trial_likelihood} and \ref{eqn:trial_omega}.
For each discrete predictor variable, we computed the fraction of successes (marginalized over the other predictors) from each posterior predictive dataset, generating a posterior predictive distribution for each predictor variable.
We then compared the posterior predictive fraction of successes to the observed fraction of successes for each predictor variable; if our model provides an adequate description of the relationship between the predictor variables and the outcome, then the observed fraction of successes for a particular predictor variable should be near the center of its respective posterior predictive distribution.
We performed this analysis separately for the \emph{test} and \emph{control} datasets.
With few exceptions, our model provided a very good description of the relationship between outcomes and predictor variables for both datasets (Figure \ref{fig:test_pps}).

% We then computed the distribution of the fraction of successes (\IE the number of times diversity-dependence was detected) for each predictor predictor variable across posterior predictive datasets

%each of which exactly matched the composition of the logistic regression analysis (\emph{e.g.} same number of phylogenies tested, same distribution of tree sizes). 
%We chose to sample from the joint \emph{tree size} and \emph{sampling fraction} distribution. 
%For each simulated dataset, we recorded the prevalence of diversification-rate decreases for each detection method in Table \ref{tab:results_summary} ($\gamma$-statistic, $\gamma$-statistic with diversified sampling, \verb!laser!, \verb!laser! with diversified sampling, \verb!TreePar!, and \verb!expoTree!).

\clearpage
\renewcommand\bibsection{\section*{Supplementary References}}

\def\bibindent{1em}
\begin{thebibliography}{99\kern\bibindent}

\bibitem[{Akaike(1974)}]{akaike1974}
Akaike, H. 1974. A new look at the statistical model identification. Automatic
  Control, IEEE Transactions on 19:716--723.

\bibitem[{Brock et~al.(2011)Brock, Harmon, and Alfaro}]{Brock2011}
Brock, C.~D., L.~J. Harmon, and M.~E. Alfaro. 2011. Testing for temporal
  variation in diversification rates when sampling is incomplete and nonrandom.
  Systematic Biology 60:410–--419.

\bibitem[{Brooks and Gelman(1998)}]{Brooks1998b}
Brooks, S. and A.~Gelman. 1998. {General methods for monitoring convergence of
  iterative simulations}. Journal of Computational and Graphical Statistics
  7:434--455.

\bibitem[{Cusimano et~al.(2012)Cusimano, Stadler, and Renner}]{Cusimano2012}
Cusimano, N., T.~Stadler, and S.~S. Renner. 2012. A new method for handling
  missing species in diversification analysis applicable to randomly or
  nonrandomly sampled phylogenies. Systematic Biology 61:785--792.

\bibitem[{Darriba et~al.(2012)Darriba, Taboada, Doallo, and
  Posada}]{darriba2012}
Darriba, D., G.~L. Taboada, R.~Doallo, and D.~Posada. 2012. {jModelTest} 2:
  more models, new heuristics and parallel computing. Nature Methods 9:772.

\bibitem[{Drummond et~al.(2012)Drummond, Suchard, Xie, and
  Rambaut}]{Drummond2012}
Drummond, A.~J., M.~A. Suchard, D.~Xie, and A.~Rambaut. 2012. Bayesian
  phylogenetics with {BEAUti} and the {BEAST} 1.7. Molecular Biology and
  Evolution 29:1969--1973.

\bibitem[{Etienne et~al.(2012)Etienne, Haegeman, Stadler, Aze, Pearson, Purvis,
  and Phillimore}]{Etienne2012a}
Etienne, R., B.~Haegeman, T.~Stadler, T.~Aze, P.~Pearson, A.~Purvis, and
  A.~Phillimore. 2012. Diversity-dependence brings molecular phylogenies closer
  to agreement with the fossil record. Proceedings of the Royal Society B:
  Biological Sciences 279:1300--1309.

\bibitem[{Etienne et~al.(2016)Etienne, Pigot, and Phillimore}]{Etienne2016}
Etienne, R.~S., A.~L. Pigot, and A.~B. Phillimore. 2016. How reliably can we
  infer diversity-dependent diversification from phylogenies? Methods in
  Ecology and Evolution 7:(in press).

\bibitem[{Gelman and Rubin(1992)}]{Gelman1992a}
Gelman, A. and D.~Rubin. 1992. {Inference from iterative simulation using
  multiple sequences}. Statistical Science 7:457--472.

\bibitem[{Geweke(1992)}]{Geweke1992}
Geweke, J. 1992. Evaluating the accuracy of sampling-based approaches to the
  calculation of posterior moments (with discussion). Pages~169--193 \emph{in}
  {B}ayesian Statistics 4 (J.~Bernardo, J.~Berger, A.~Dawid, and A.~Smith,
  eds.). Oxford University Press, Oxford.

\bibitem[{H\"ohna et~al.(2011)H\"ohna, Stadler, Ronquist, and
  Britton}]{hohna2011}
H\"ohna, S., T.~Stadler, F.~Ronquist, and T.~Britton. 2011. {Inferring
  speciation and extinction rates under different species sampling schemes}.
  Molecular Biology and Evolution 28:2577--2589.

\bibitem[{Lanfear et~al.(2012)Lanfear, Calcott, Ho, and Guindon}]{lanfear2012}
Lanfear, R., B.~Calcott, S.~Y.~W. Ho, and S.~Guindon. 2012. {PartitionFinder}:
  Combined selection of partitioning schemes and substitution models for
  phylogenetic analyses. Molecular Biology and Evolution 29:1695--1701.

\bibitem[{Leventhal et~al.(2014)Leventhal, G{\"u}nthard, Bonhoeffer, and
  Stadler}]{Leventhal2014}
Leventhal, G.~E., H.~F. G{\"u}nthard, S.~Bonhoeffer, and T.~Stadler. 2014.
  Using an epidemiological model for phylogenetic inference reveals density
  dependence in {HIV} transmission. Molecular Biology and Evolution 31:6--17.

\bibitem[{Magee et~al.(2014)Magee, May, and Moore}]{Magee2014}
Magee, A.~F., M.~R. May, and B.~R. Moore. 2014. The dawn of open access to
  phylogenetic data. PLoS ONE 9:e110268.

\bibitem[{McPeek(2008)}]{McPeek2008}
McPeek, M.~A. 2008. The ecological dynamics of clade diversification and
  community assembly. The American Naturalist 172:E270--E284.

\bibitem[{Paradis(1997)}]{Paradis1997}
Paradis, E. 1997. Assessing temporal variations in diversification rates from
  phylogenies: estimation and hypothesis testing. Proceedings of the Royal
  Society of London B: Biological Sciences 264:1141--1147.

\bibitem[{Paradis et~al.(2004)Paradis, Claude, and Strimmer}]{ape}
Paradis, E., J.~Claude, and K.~Strimmer. 2004. A{PE}: analyses of phylogenetics
  and evolution in {R} language. Bioinformatics 20:289--290.

\bibitem[{Pennell et~al.(2012)Pennell, Sarver, and Harmon}]{pennell2012}
Pennell, M.~W., B.~A.~J. Sarver, and L.~J. Harmon. 2012. Trees of unusual size:
  Biased inference of early bursts from large molecular phylogenies. PLoS ONE
  7:1--7.

\bibitem[{Plummer et~al.(2006)Plummer, Best, Cowles, and Vines}]{Plummer2006}
Plummer, M., N.~Best, K.~Cowles, and K.~Vines. 2006. {CODA: Convergence
  diagnosis and output analysis for MCMC}. R News 6:7--11.

\bibitem[{Polson et~al.(2013)Polson, Scott, and Windle}]{Polson2013}
Polson, N.~G., J.~G. Scott, and J.~Windle. 2013. Bayesian inference for
  logistic models using {P}{\'o}lya--gamma latent variables. Journal of the
  American Statistical Association 108:1339--1349.

\bibitem[{Pybus and Harvey(2000)}]{Pybus2000}
Pybus, O.~G. and P.~H. Harvey. 2000. Testing macro-evolutionary models using
  incomplete molecular phylogenies. Proceedings of the Royal Society B:
  Biological Sciences 267:2267--72.

\bibitem[{Rabosky(2006{\natexlab{a}})}]{Rabosky2006}
Rabosky, D. 2006{\natexlab{a}}. Likelihood methods for detecting temporal
  shifts in diversification rates. Evolution 60:1152--1164.

\bibitem[{Rabosky(2006{\natexlab{b}})}]{Rabosky2006b}
Rabosky, D.~L. 2006{\natexlab{b}}. {LASER}: a maximum likelihood toolkit for
  detecting temporal shifts in diversification rates from molecular
  phylogenies. Evolutionary Bioinformatics Online 2:247.

\bibitem[{Revell et~al.(2005)Revell, Harmon, and Glor}]{Revell2005}
Revell, L.~J., L.~J. Harmon, and R.~E. Glor. 2005. Under-parameterized model of
  sequence evolution leads to bias in the estimation of diversification rates
  from molecular phylogenies. Systematic Biology 54:973--983.

\bibitem[{Sanderson et~al.(1994)Sanderson, Donoghue, Piel, and
  Eriksson}]{Sanderson1994b}
Sanderson, M., M.~Donoghue, W.~Piel, and T.~Eriksson. 1994. {TreeBASE}: a
  prototype database of phylogenetic analyses and an interactive tool for
  browsing the phylogeny of life. American Journal of Botany 81:183.

\bibitem[{Sanderson(1997)}]{sanderson97}
Sanderson, M.~J. 1997. A nonparametric approach to estimating divergence times
  in the absence of rate constancy. Molecular Biology and Evolution
  14:1218--1231.

\bibitem[{Sanderson(2002)}]{sanderson02}
Sanderson, M.~J. 2002. Estimating absolute rates of molecular evolution and
  divergence times: a penalized likelihood approach. Molecular Biology and
  Evolution 19:101--109.

\bibitem[{Stadler(2011)}]{Stadler2011}
Stadler, T. 2011. Mammalian phylogeny reveals recent diversification rate
  shifts. Proceedings of the National Academy of Sciences, USA 108:6187--6192.

\bibitem[{Zwickl(2006)}]{zwickl06}
Zwickl, D.~J. 2006. Genetic algorithm approaches for the phylogenetic analyses
  of large biological sequence datasets under the maximum likelihood criterion.
  Ph.D. dissertation, The University of Texas at Austin
  www.bio.utexas.edu/faculty/antisense/garli/Garli.html.


\end{thebibliography}

%\bibliography{bayes.bib}

%\section*{Figures}

\begin{figure}[H]
\begin{center}
\includegraphics[width=0.9\linewidth]{./figures/analysis_pipeline.pdf}
\end{center}
\caption{All steps required to quantify the factors affecting the prevalence diversity-dependent diversification. Step labels on left side correspond to subsections of the Supplementary Methods.}
\label{fig:analysis_pipeline}
\end{figure}

\begin{figure}[H]
\begin{center}
\includegraphics[width=0.9\linewidth]{./figures/descriptive_histograms.pdf}
\end{center}
\caption{Histograms for the phylogenies in the emph{test} (green) and \emph{control} (purple) datasets showing (from left to right) the size, the sampling fraction, and the percentage of resolved.}
\label{fig:analysis_pipeline}
\end{figure}

%\begin{figure}[H]
%\begin{center}
%\includegraphics[width=0.9\linewidth]{./figures/size_histogram.pdf}
%\end{center}
%\caption{Histograms of the sizes of phylogenies comprising the \emph{test} (green) and \emph{control} (purple) datasets.}
%\label{fig:tree_size_hist}
%\end{figure}
%
%\begin{figure}[H]
%\begin{center}
%\includegraphics[width=0.9\linewidth]{./figures/rho_histogram.pdf}
%\end{center}
%\caption{The distribution of the sampling fractions of the phylogenies comprising the \emph{test} (green) and \emph{control} (purple) datasets.}
%\label{fig:rho_hist}
%\end{figure}
%
%\begin{figure}[H]
%\begin{center}
%\includegraphics[width=0.9\linewidth]{./figures/resolution_histogram.pdf}
%\end{center}
%\caption{The distribution of the percentage of binary internal nodes in the phylogenies comprising the \emph{test} (green) and \emph{control} (purple) datasets.}
%\label{fig:resolution_hist}
%\end{figure}

\clearpage
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.9\linewidth]{./figures/effect_size.pdf}
\end{center}
\caption{
Posterior distributions for the effect size of various predictors estimated using Bayesian logistic regression on the \emph{test} dataset.
Horizontal bars are the 95\% HPD intervals.
Positive values of the effect size indicate that the predictor increases the probability of inferring diversification-rate decreases, while a negative effect size indicates a decreased probability.}
\label{fig:effect_size}
\end{figure}
%test_betas

\clearpage
\begin{comment}
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.9\linewidth]{./figures/control_betas.pdf}
\end{center}
\caption{
Posterior distributions for the effect size of various predictors estimated using Bayesian logistic regression on the \emph{control} dataset.
Horizontal bars are the 95\% HPD intervals.
Positive values of the effect size indicate that the predictor increases the probability of inferring diversification-rate decreases, while a negative effect size indicates a decreased probability.}
\label{fig:control_betas}
\end{figure}
\end{comment}

\begin{figure}[H]
\begin{center}
\includegraphics[width=0.9\linewidth]{./figures/model_adequacy.pdf}
\end{center}
\caption{
Results from posterior predictive simulation for phylogenies from the \emphp{test} dataset.
The orange shapes are the posterior predictive densities for the probabilities of a given phylogeny (given a divergence time estimation method and an analysis method) displaying a diversification-rate decrease.
The black stars are the observed values.}
\label{fig:test_pps}
\end{figure}

\begin{comment}
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.9\linewidth]{./figures/control_pps.pdf}
\end{center}
\caption{
Results from posterior predictive simulation for phylogenies from the \emphp{control} dataset.
The orange shapes are the posterior predictive densities for the probabilities of a given phylogeny (given a divergence time estimation method and an analysis method) displaying a diversification-rate decrease.
The black stars are the observed values.}
\label{fig:control_pps}
\end{figure}
\end{comment}

\begin{figure}[H]
\begin{center}
\includegraphics[width=\textwidth]{./figures/heatmap_combined_2.pdf}
\end{center}
\caption{Heat maps of the results for each analysis method and dataset (excluding the test dataset analyzed with non-parametric rate smoothing, Figure \ref{fig:heatmap_test_nprs}).
		 For each heat map, the rows are the phylogenies, the columns are, from left to right, the $\gamma$-statistic, the $\gamma$-statistic employing the diversified model of taxon sampling, \laser, \laser employing the diversified model of taxon sampling, \treepar, and \expotree. A cell is orange if that method detected a decrease in diversification on that phylogeny (diversity-dependent or otherwise), green if it detected an increase, blue if it failed to reject constant rates, and grey if the analysis failed.}
\label{fig:heatmap_combined}
\end{figure}