\documentclass[11pt]{article}
\usepackage{amsmath, amsfonts, amssymb, graphicx, mathrsfs}
\usepackage[round]{natbib}
\usepackage[margin=1in]{geometry}
\usepackage[usenames,dvipsnames,svgnames,table]{xcolor}
\usepackage{rotating}
\usepackage{rotfloat}
\usepackage{booktabs}
\usepackage{lineno}
\usepackage{enumitem}
\usepackage{paralist}
\usepackage{pdflscape}
\usepackage{setspace}
\usepackage[font=footnotesize,labelfont=bf]{caption}
\usepackage{colortbl}
\usepackage[table]{xcolor}
\usepackage{verbatim}
\usepackage{longtable}
\usepackage{tabu}
\usepackage{float}
\usepackage{titlesec}
\usepackage{lineno}
\usepackage{fancyhdr}
\usepackage{tocloft}% http://ctan.org/pkg/tocloft
\usepackage{multirow}
\usepackage{xspace}
\usepackage{hyperref}

\definecolor{lightgray}{HTML}{E8E8E8}
\definecolor{darkgray}{HTML}{DBDBDB}
\newcommand{\IE}{{\it i.e.,}\xspace}
\newcommand{\EG}{{\it e.g.,}\xspace}
\newcommand{\CF}{{\it c.f.,}\xspace}

\newcommand{\laser}{\texttt{laser}\xspace}
\newcommand{\treepar}{\texttt{TreePar}\xspace}
\newcommand{\expotree}{\texttt{expoTree}\xspace}
\newcommand{\gammastat}{$\gamma$-statistic\xspace}

\newcommand{\mrmadd}[1]{{\color{orange}#1}}


\bibliographystyle{sysbio.bst}

\setlength{\cftsecnumwidth}{2em}
\setlength{\cftsubsecnumwidth}{2.5em}
\setlength{\cftsubsubsecnumwidth}{3.2em}

\setcounter{section}{0}
\setcounter{table}{0}
\setcounter{figure}{0}
\setcounter{equation}{0}
\renewcommand{\thesection}{SI.\arabic{section}}
\renewcommand{\theequation}{S.\arabic{equation}}
\renewcommand{\thefigure}{S\arabic{figure}}

\begin{document}

\section*{Supporting Information}
\renewcommand{\thetable}{S.\arabic{table}}
\renewcommand{\thefigure}{S.\arabic{figure}}

\subsection*{Methods}

\subsubsection*{Data source}
The phylogenies analyzed in these studies come from three distinct sources: two published papers \citep{McPeek2008,Magee2014} and a novel set obtained specifically for this study as a control.
Potential phylogenies for the control dataset were selected from studies published in Systematic Biology between 2003 and 2013.
Alignments were downloaded from TreeBASE \citep{Sanderson1994b} when available, and inspected to determine whether there was sufficient information present to partition the alignment in the paper of origin and the nexus alignment. If a study specified that an alignment had been partitioned and we could not obtain enough information to recreate this partitioning scheme, we discarded it. 
If the study from which the phylogeny originated conducted an unpartitioned analysis, we used jModelTest 2.1.7 \citep{darriba2012} to determine the best fitting model of substitution for the entire alignment. 
For those phylogenies that had originally been partitioned, we used PartitionFinder 1.1.1 \citep{lanfear2012} to determine the best partitioning scheme, using the greedy algorithm, setting the model selection criterion to BIC, and setting the original study's partitions as the data subsets.
We specified that PartitionFinder should consider all available DNA substitution models except combinations with both gamma-distributed rate variation and a proportion of invariant sites, due to generally weak identifiability of this mixture model. 
For consistency, we post-processed the output from jModelTest and removed from consideration all models that we did not specify in PartitionFinder.
Phylogenies were then inferred using GARLI 2.01 \citep{zwickl06}, specifying the best-fit substitution models and partitioning schemes, 5 separate searches (searchreps=5), and otherwise default settings. 

\subsubsection*{Dataset curation}
All phylogenies were inspected to ensure that they were species-level, taxonomically distinct from other studies, and did not contain an excessive fraction of unresolved internal nodes (polytomies). 
To assess whether phylogenies were at the species level, the names of species in each phylogeny were checked for uniqueness using a combination of \verb!R! scripting and manual investigation. 
When a phylogeny appeared to have multiple representatives of the same species, the original study was consulted. 
Unless there was convincing evidence that a study believed multiple representatives of a species should be included in that phylogeny, tips were pruned until only one tip per species remained. 
Convincing evidence meant either a statement in the study (\EG acknowledging that the species was found not to be monophyletic or suggesting that the species was likely a cryptic species complex) or a figure showing a resolved node separating the representatives of the species (\textit{i.e.} the tips were not descended from a polytomy). 
The taxonomic uniqueness of phylogenies was checked first by considering the names of the focal clades for the phylogenies. 
Then, the names of the genera included in all phylogenies were extracted and compared against each other using custom \verb!R! scripts.
A list of phylogenies that contained shared genus names was generated, and those phylogenies were checked manually. 
In a case where two phylogenies were found to overlap taxonomically---whether they included the same species, genus, or higher taxon---the less fully sampled phylogeny was removed from the dataset. 
Phylogenies were inspected manually for polytomies, and any phylogeny containing a large proportion of unresolved internal nodes was removed. 

To guarantee that the sampling fraction (the number of species in the phylogeny divided by the number of extant species descended from the MRCA) for all phylogenies was calculated consistently, during the curation process, the true number of species in each clade was recorded. 
For consistency with the original studies, if there was a stated estimate of this diversity, that number was used. 
In cases where this number could not be obtained directly from a study, we consulted Wikipedia for information, and if that failed we performed a Google search. 
If necessary, the true size of the clade was estimated by summing over the diversities of clades found to be included in the phylogeny. 
When multiple representatives of a species were included in a study, or if a new species was suggested, the true clade size was adjusted accordingly. 

\subsubsection*{Divergence-time estimation and categorization}
There are a number of available methods that can be employed to estimate the divergence times of a phylogram without the use of sequence data. 
%In his 2008 study \citep{mcpeek2008}, McPeek used Mean Path Length dating \citep{britton2002} to estimate divergence times, having gathered the phylogenies from printed copies using calipers and hand-writing newick strings. 
%When we used MPL the non-ultrametric trees obtained from McPeek, we found that MPL generated surprising number of phylogenies with negative branch lengths, which are biologically impossible. 
%Both the Yule and the birth-death processes assume that time moves in a single direction, and since every method that can be used to test a tree depends on one of these models, we chose not to employ MPL on any of our phylogenies.
To assess the effect of different divergence time estimators on the prevalence of diversification-rate decreases, we employed both non-parametric rate smoothing (NPRS) \citep{sanderson97} and penalized likelihood (PL) \citep{sanderson02} to make McPeek's phylogenies ultrametric.
For consistency, we also used these methods on the \emph{control} phylogenies.
Penalized likelihood was employed using the \texttt{chronopl} function implemented in the \verb!R! package \verb!ape!, employing cross validation and setting lambda (the so-called smoothing parameter) to 1. 
NPRS is no longer a feature included in current releases of \verb!ape!, so to implement it we downloaded an archived version, 2.3-1, from CRAN, obtained all relevant \verb!C! and \verb!R! code and compiled the function locally. 
The phylogenies obtained from \citet{Magee2014} were already ultrametric, so they were categorized by how the original studies estimated divergence times. 
In cases where it was unclear how a study estimated divergence times, the phylogeny was discarded.
To ensure that there was enough information to estimate logistic regression parameters for the divergence time estimators, we only included methods that were applied to at least five phylogenies, which were BEAST, NPRS, and PL.

\subsubsection*{Diversification-through time analyses}
There are a number of methods that have been described for testing whether a phylogeny has experienced temporal variation in diversification rates, most of of which are implemented in \texttt{R} packages. 
The most popular of these is the $\gamma$-statistic \citep{Pybus2000}, implemented in numerous \texttt{R} packages, including \verb!ape! \citep{ape}. 
Also popular is Rabosky's birth-death likelihood method \citep{Rabosky2006}, implemented in the package \verb!laser! \citep{Rabosky2006b}. 
Less widely used methods include Paradis' Survival Analysis \citep{Paradis1997}, implemented in  \verb!ape!, Etienne's diversity-dependent diversification likelihood method \citep{Etienne2012a}, implemented in the package \verb!DDD!, Stadler's birth-death with shifts method \citep{Stadler2011}, implemented in \verb!TreePar!, and Leventhal's epidemiological diversity-dependent likelihood method \citep{Leventhal2014}, implemented in the package \verb!expoTree!.
Survival Analysis assumes that phylogenies diversified under a pure-birth process, potentially leading to spurious detection of variation in diversification rate variation \citep{Pybus2000}, and thus we did not apply it to our phylogenies.
While Etienne's package \verb!DDD! allows for an impressive flexibility in both models of diversity-dependence and time-dependence, the length of its run time precluded our use in this analysis. 

The $\gamma$-statistic is a normalized weighted average of the branching times in a phylogeny.
If the branching times in a phylogeny fall closer to the root than expected under a pure-birth (Yule) process, the $\gamma$-statistic is negative.
% Fortunately, extinction does not affect the performance of the $\gamma$-statistic.
If the $\gamma$-statistic is significantly negative (under the standard normal), the null hypothesis of a constant rate of diversification is rejected.
A significantly negative $\gamma$ value does not imply what form the decrease takes; that is, it cannot discriminate between a time-dependent model and a diversity-dependent model as birth-death likelihood can.
If a phylogeny has incomplete taxon sampling, the $\gamma$-statistic will be biased towards detecting decreases \citep{Pybus2000}. 
Therefore, in the presence of incomplete species sampling, Monte Carlo simulation must be employed to compute the correct null distribution for the test statitic.
For each phylogeny we analyzed, we simulated 10,000 pure-birth phylogenies with the same size and sampling fraction as the empirical phylogeny, calculated the gamma statistic for each, and used this null distribution to assess the significance of the $\gamma$ values.
When simulating the null distribution, any model of taxon sampling can be used to account for incomplete sampling, but historically sampling has always been assumed to be random.
Nonrandom sampling can cause a severe negative bias in the value of $\gamma$ that is not captured by the random sampling model, leading to inflated Type 1 error.
To address this issue, we also simulated phylogenies using H{\"o}hna et al.'s diversified model \citep{hohna2011}, which assumes that the taxa present in a phylogeny represent the oldest possible divergences for a given sampling fraction.
While obviously an oversimplification of reality, this model allows us to set a lower limit on the prevalence of diversity-dependence, while the uniform model sets the upper limit.

Rabosky's birth-death likelihood approach \citep[\laser,][]{Rabosky2006} allows a user to consider a range of time-dependent and diversity-dependent models of diversification. 
To perform model selection, \verb!laser! uses a statistic based on the AIC \citep{akaike1974}, the $\Delta$AIC$_{RC}$, calculated by subtracting the AIC score from the best fitting variable rate model from the AIC score of the best fitting constant rate model. 
If the $\Delta$AIC$_{RC}$ is positive, we reject constant-rate models in favor of variable-rate models and take the variable-rate model with the best AIC score to be the best model of diversification for the observed phylogeny. 
Following convention, we implemented two constant-rate models, the Yule (pure birth) and birth-death, and three variable-rate models, the Yule-two-rate (which divides the tree into two time windows and fits a different Yule model to each), DDL (where the effect of diversity-dependence is logistic), and DDX (where the effect of diversity-dependence is exponential). 
It is also possible to use the $\Delta$AIC$_{RC}$ as a test-statistic and simulate a null distribution (that is, to perform Monte Carlo simulation), which allows us to account for incomplete taxon sampling using both the uniform random and diversified models of taxon sampling as with the $\gamma$-statistic. 

The episodic birth-death likelihood approach \citep[\treepar,][]{Stadler2011} divides a phylogeny into some number of windows in time, each of which is governed by a different set of birth-death parameters. 
The user specifies the maximum number of rate shifts, \emph{n}.
\treepar then discretizes the potential locations for rate shifts on the phylogeny and evaluates all possible placements of rate shifts. 
\treepar provides the likelihoods (and estimated parameters) for models with 0 (constant diversification) up to \emph{n} rate shifts.
To determine the number of rate shifts that best describes the phylogeny, Hierarchical Likelihood Ratio tests are employed, starting with a constant rate tree and continuing up to the maximum number of breakpoints considered.
To allow for a simple classification of a tree as either displaying a decrease or not, we chose \emph{n} = 1, such that we compared a constant rate model to a two-rate model. 
We specified 10 possible shift locations per phylogeny, distributed uniformly in time.
As implemented in \verb!TreePar!, the episodic birth-death likelihood approach accommodates missing taxa under the assumption that they were randomly sampled (\IE it does not accommodate non-random taxon sampling).

The epidemiological diversity-dependent likelihood approach derives the likelihood of a phylogeny under a stochastic Susceptible-Infected-Susceptible model, with or without a diversity-dependence parameter.
The method can be applied to reconstructed species-level phylogenies when the rate of serial sampling is set to 0 (which in this context indicates the rate at which fossil species are recovered from the rock record), and by treating transmission events as speciation events and death/recovery events as extinctions.
We chose not to use the inbuilt functions \verb!expoTree.optim!  to fit the diversity-dependent and constant models, which are designed to work better with epidemiological than species phylogenies (Leventhal, personal communication).
We followed the original implementation in calling \verb!optim()! using the \verb!Nelder-Mead! optimization method, fixing the sampling fraction, $\rho$ to the observed value, and fixing $\psi$---the epidemiological parameter for sequential sampling---to 0.
As implemented in \verb!expoTree!, the epidemiological diversity-dependent likelihood approach accommodates missing taxa under the assumption that they were randomly sampled.

Each of the four methods we implemented have both strengths and weaknesses, summarized in Table \ref{tab:method_summary}. 
Due to the use of Monte Carlo simulation, the $\gamma$-statistic and birth-death likelihood have the advantage of being able to incorporate different models of taxon sampling.
The $\gamma$-statistic also has the advantage of being quite well characterized for biases \citep[\CF][]{Revell2005,Brock2011,Cusimano2012,pennell2012}, but the lack of explicit model renders it incapable of discriminating between diversity-dependent and prosaic in diversification-rate variation.
The only models implemented in birth-death likelihood of diversity-dependence are strictly pure-birth, and as conventionally implemented, it includes no variable-rate model with extinction.
The episodic birth-death approach has the advantage of explicitly and directly addressing both missing species and extinction, though it cannot detect diversity-dependence directly, so it serves to provide a more generous estimate of the maximum prevalence of diversity-dependence.
The epidemiological diversity-dependent likelihood approach is the only method we used that accounts for extinction and diversity-dependence concurrently, though in having no model of time-dependent diversification, it is likely identifying diversity-dependence in cases where the true best model is time-dependent. 
Recent work \citep{Etienne2016} suggests that the epidemiological diversity-dependent approach, by merit of using likelihood-ratio tests, suffers from inflated Type 1 error.
It is possible that this also effects the episodic birth-death approach.
This suggests that the upper limit on the prevalence of diversity-dependence established by both of these methods are perhaps very generous, but does not undermine the uncertainty surrounding the prevalence of diversity-dependence.

\begin{table}[H]
\caption{\bf{Characteristics of methods used to detect temporal diversification-rate variation.}}\label{tab:method_summary}
\label{tab:method_capabilities}
\centering
\begin{tabular} {lcccc}
\toprule
 	   					& explicit diversity-dependent 	& time-dependent 	& \multicolumn{2}{c}{species-sampling model}\\
method 					& model 				  	& model	 		& random			&  diversified			\\
\midrule
\texttt{$\gamma$-statistic} 	& no 					& yes 			& yes 			& yes				\\
\rowcolor{gray!25}
\texttt{laser} 				& yes 					& yes  			& yes			& yes				\\
\texttt{TreePar} 			& no 					& yes 			& yes 			& no					\\
\rowcolor{gray!25}
\texttt{expoTree} 			& yes 					& no 			& yes 			& no					\\
\bottomrule
\end{tabular}
\end{table}


\subsubsection*{Logistic regression analyses}
We follow the logistic regression methods in \citep{Magee2014} to understand the relationship between various biological and methodological features and the detection of diversification-rate variation through time; for completeness, we present complete details of this approach here.
We used Bayesian logistic regression to explore correlations between the detection of diversification-rate decrease and several variables. 
Under this approach, a \emph{trial} is a diversification-through time analysis of a given phylogeny---made ultrametric with a particular method---using a particular diversification-rate through time analysis method, which we deem a \emph{success} if that phylogeny displays a diversification-rate decrease. 
The outcomes of a set of $n$ trials are contained in a data vector $\boldsymbol{x} = \{x_1,x_2,\ldots,x_n\}$, where $x_i$ is 1 if phylogeny $i$ displays a diversification-rate decrease and is 0 otherwise. 
The outcome of each trial depends on a set of $k$ \emph{predictor variables} that may be numerical (\emph{e.g.}, the size of the phylogeny) or categorical (\emph{e.g.}, whether taxon sampling is assumed to be uniform or diversified). 
An $n \times k$ matrix $\mathcal{I}$, the \emph{design matrix}, describes the relationships between trials and predictor variables: $\mathcal{I}_{ij}$ is the value for predictor variable $j$ for trial $i$. 
\emph{Parameters} relate the values of each predictor variable to the probability of success of each trial, and are described by the parameter vector $\boldsymbol\beta = \{\beta_1,\beta_2,\ldots,\beta_k\}$, where $\beta_i$ is the contribution of parameter $i$ to the probability of success.

In a Bayesian framework, we are interested in estimating the joint posterior probability distribution of the model parameters $\boldsymbol\beta$ conditional on the data $\boldsymbol{x}$. 
According to Bayes' theorem,
\begin{align*}
P(\boldsymbol\beta \mid \boldsymbol x) = \frac{ P( \boldsymbol{x} \mid \boldsymbol\beta  ) P(\boldsymbol\beta) }{ \int P( \boldsymbol{x} \mid \boldsymbol\beta  ) P(\boldsymbol\beta)\ \mathrm{d}\boldsymbol\beta },
\end{align*}
the \emph{posterior probability} of the model parameters, $P(\boldsymbol\beta \mid \boldsymbol x)$, is equal to \emph{likelihood} of the data given the model parameters, $P(\boldsymbol{x} \mid \boldsymbol\beta)$, multiplied by the \emph{prior probability} of the parameters, $P(\boldsymbol\beta)$, divided by the \emph{marginal likelihood} of the data.

Given the design matrix $\mathcal{I}$, the outcomes of each of the $n$ trials are conditionally independent, so that the likelihood of $\boldsymbol x$ is the product of the likelihoods for each individual trial:
\begin{align*}
P(\boldsymbol{x} \mid \boldsymbol\beta) = \prod_{i=1}^n P(x_i \mid \mathcal{I}, \boldsymbol\beta).
\end{align*}
The likelihood of observing the outcome of a particular trial is
\begin{align}\label{eqn:trial_likelihood}
P(x_i \mid \mathcal{I}, \boldsymbol\beta) = \begin{cases}
									\frac{1}{1+e^{-\omega_i} } & \text{if } x_i=1\\
									1-\frac{1}{1+e^{-\omega_i}} & \text{if } x_i=0,
								  \end{cases}
\end{align}
where
\begin{align}\label{eqn:trial_omega}
\omega_i = \sum_{j=1}^{k} \mathcal{I}_{ij}\beta_j.
\end{align}

We specified a multivariate normal prior probability distribution on the $\boldsymbol\beta$ parameters with means $\boldsymbol\mu$ and covariance matrix $\Sigma$. 
The complexity of the marginal likelihood precludes an analytical solution to the posterior probability distribution. 
Accordingly, we approximated the posterior probability distribution using the Markov chain Monte Carlo algorithm implemented in  the  \verb!R!  package \verb!BayesLogit! \citep{Polson2013}. 
This program uses conjugate prior and posterior probability distributions (via Polya-gamma-distributed latent variables), which permits use of an efficient Gibbs sampling algorithm to approximate the joint posterior distribution of $\boldsymbol\beta$ conditional on the data. 

We defined a set of predictor variables based on factors known to affect diversification-rate analyses.
We included an \emph{intercept} predictor variable to describe the background probability of detecting a decrease.
We treated \emph{tree size} and \emph{sampling fraction} as (mean-centered) continuous predictor variables, and \emph{diversification-rate analysis method}, \emph{sampling model}, and \emph{divergence time estimator} as discrete predictor variables.

Discrete predictor variables for logistic regression are generally binary, assuming values of 0 or 1. 
Aside from the model of taxon sampling, our discrete variables had more than two possible categories. 
We therefore adopted an \emph{indicator-variable} approach in which predictor variables with $p$ categories are discretized into $p$ distinct indicators; each phylogeny in a particular predictor category was then assigned a 1 for the corresponding indicator variable. 
Under this approach, phylogenies analyzed with the $\gamma$-statistic were assigned a 1 for the \emph{$\gamma$-statistic} variable, phylogenies analyzed with laser were assigned a 1 for the \emph{laser} variable, and phylogenies analyzed with expotree were assigned a 1 for the \emph{$\gamma$-statistic} variable. 
For the analysis of the control dataset, phylogenies made ultrametric with PL were assigned a 1 for the \emph{PL} variable. 
For the analysis of the ``test'' dataset, a unique indicator was assigned to each divergence time estimator in both the Magee et al. 
phylogenies and the McPeek trees to account for between dataset-heterogeneity in the inferred prevalence of diversification-rate decreases. 

Phylogenies made ultrametric with BEAST in the Magee dataset were assigned a 1 for the \emph{BEAST$_\text{Magee}$} variable, phylogenies made ultrametric with PL in the Magee dataset were assigned a 1 for the \emph{PL$_\text{Magee}$} variable, phylogenies made ultrametric with NPRS in the Magee dataset were assigned a 1 for the \emph{NPRS$_\text{Magee}$} variable, and phylogenies made ultrametric with PL in the McPeek dataset were assigned a 1 for the \emph{PL$_\text{McPeek}$} variable.
In order to avoid overparameteriziation of the logistic model, we did not assign indicator variables for the \emph{TreePar} variable in either analysis, the \emph{NPRS} variable in the control analysis, and the \emph{NPRS$_\text{McPeek}$} variable in the ``test'' analysis. 
Accordingly, the values for  $\gamma$-\emph{statistic}, \emph{laser}, and \emph{expotree} parameters are interpreted as effects relative to TreePar; similarly, the value for \emph{PL} is interpreted as the effect relative to NPRS in the control dataset, and \emph{BEAST$_\text{Magee}$}, \emph{PL$_\text{Magee}$}, \emph{NPRS$_\text{Magee}$}, and \emph{PL$_\text{McPeek}$} variables are interpreted as the effect relative to NPRS in the McPeek dataset for the \emph{test} analysis. 

We estimated parameters for each data subset by performing four independent MCMC simulations, running each chain for $2.5 \times 10^5$ cycles plus $10^3$ cycles as burnin. 
We assessed the performance of all MCMC simulations using the \texttt{Tracer} \citep[][]{Drummond2012} and \texttt{coda} \citep{Plummer2006} packages.
We monitored convergence of each chain to the stationary distribution by plotting the time series and calculating the Geweke diagnostic \citep[$p$,][]{Geweke1992} for every parameter.
We assessed the mixing of each chain over the stationary distribution by calculating both the potential scale reduction factor \citep[\emph{PSRF},][]{Gelman1992a} diagnostic and the effective sample size \citep[\emph{ESS}][]{Brooks1998b} for all parameters.
Values of all diagnostics for all parameters in all MCMC simulations indicate reliable approximation of the stationary (joint posterior probability) distributions: \EG $\text{\emph{ESS}} > 60000$; $\text{\emph{PSRF}} = 1$; $p > 0.05$ for each parameter.
Additionally, we assessed convergence by comparing the four independent estimates of the marginal posterior probability density for each parameter, ensuring that all parameter estimates were effectively identical and SAE compliant \citep{Brooks1998b}.
Based on these diagnostic analyses, we based parameter estimates on the combined stationary samples from each of the four independent chains ($N = 10^6$).

To assess the adequacy of our logistic regression model, we performed posterior predictive simulations for both the \emph{test} and \emph{control} analyses. 
Each simulation consisted of a randomly drawing a vector of parameters, $\boldsymbol\beta_\text{sim}$, from the approximate joint posterior distribution generated by our logistic regression analysis.
For each random draw from the joint posterior distribution, we simulated a new posterior predictive dataset, where each element of the dataset was a success or failure according to $\boldsymbol\beta_\text{sim}$ and $\mathcal{I}$, using equations \ref{eqn:trial_likelihood} and \ref{eqn:trial_omega}.
For each discrete predictor variable, we computed the fraction of successes (marginalized over the other predictors) from each posterior predictive dataset, generating a posterior predictive distribution for each predictor variable.
We then compared the posterior predictive fraction of successes to the observed fraction of successes for each predictor variable; if our model provides an adequate description of the relationship between the predictor variables and the outcome, then the observed fraction of successes for a particular predictor variable should be near the center of its respective posterior predictive distribution.
We performed this analysis separately for the \emph{test} and \emph{control} datasets.
With few exceptions, our model provided a very good description of the relationship between outcomes and predictor variables for both datasets (Figure \ref{fig:test_pps}).

% We then computed the distribution of the fraction of successes (\IE the number of times diversity-dependence was detected) for each predictor predictor variable across posterior predictive datasets

%each of which exactly matched the composition of the logistic regression analysis (\emph{e.g.} same number of phylogenies tested, same distribution of tree sizes). 
%We chose to sample from the joint \emph{tree size} and \emph{sampling fraction} distribution. 
%For each simulated dataset, we recorded the prevalence of diversification-rate decreases for each detection method in Table \ref{tab:results_summary} ($\gamma$-statistic, $\gamma$-statistic with diversified sampling, \verb!laser!, \verb!laser! with diversified sampling, \verb!TreePar!, and \verb!expoTree!).

\clearpage
\renewcommand\bibsection{\section*{Supplementary References}}
\bibliography{bayes.bib}


\clearpage
%\section*{Figures}

\begin{figure}[H]
\begin{center}
\includegraphics[width=0.9\linewidth]{./figures/analysis_pipeline.pdf}
\end{center}
\caption{Flow chart showing how the phylogenies used in the analyses were obtained, curated, and tested.}
\label{fig:analysis_pipeline}
\end{figure}

\begin{figure}[H]
\begin{center}
\includegraphics[width=0.9\linewidth]{./figures/size_histogram.pdf}
\end{center}
\caption{Histograms of the sizes of phylogenies comprising the \emph{test} (green) and \emph{control} (purple) datasets.
		The y-axis is scaled to the density rather than the count, so that both sources are visible on the same scale.}
\label{fig:tree_size_hist}
\end{figure}

\begin{figure}[H]
\begin{center}
\includegraphics[width=0.9\linewidth]{./figures/rho_histogram.pdf}
\end{center}
\caption{The distribution of the sampling fractions of the phylogenies comprising the \emph{test} (green) and \emph{control} (purple) datasets.
		The y-axis is scaled to the density rather than the count, so that both sources are visible on the same scale.}
\label{fig:rho_hist}
\end{figure}

\begin{figure}[H]
\begin{center}
\includegraphics[width=0.9\linewidth]{./figures/rho_histogram.pdf}
\end{center}
\caption{The distribution of the percentage of binary internal nodes in the phylogenies comprising the \emph{test} (green) and \emph{control} (purple) datasets.
		The y-axis is scaled to the density rather than the count, so that both sources are visible on the same scale.}
\label{fig:resolution_hist}
\end{figure}

\clearpage
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.9\linewidth]{./figures/effect_size.pdf}
\end{center}
\caption{
Posterior distributions for the effect size of various predictors estimated using Bayesian logistic regression on the \emph{test} dataset.
Horizontal bars are the 95\% HPD intervals.
Positive values of the effect size indicate that the predictor increases the probability of inferring diversification-rate decreases, while a negative effect size indicates a decreased probability.}
\label{fig:effect_size}
\end{figure}
%test_betas

\clearpage
\begin{comment}
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.9\linewidth]{./figures/control_betas.pdf}
\end{center}
\caption{
Posterior distributions for the effect size of various predictors estimated using Bayesian logistic regression on the \emph{control} dataset.
Horizontal bars are the 95\% HPD intervals.
Positive values of the effect size indicate that the predictor increases the probability of inferring diversification-rate decreases, while a negative effect size indicates a decreased probability.}
\label{fig:control_betas}
\end{figure}
\end{comment}

\begin{figure}[H]
\begin{center}
\includegraphics[width=0.9\linewidth]{./figures/model_adequacy.pdf}
\end{center}
\caption{
Results from posterior predictive simulation for phylogenies from the \emphp{test} dataset.
The orange shapes are the posterior predictive densities for the probabilities of a given phylogeny (given a divergence time estimation method and an analysis method) displaying a diversification-rate decrease.
The black stars are the observed values.}
\label{fig:test_pps}
\end{figure}

\begin{comment}
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.9\linewidth]{./figures/control_pps.pdf}
\end{center}
\caption{
Results from posterior predictive simulation for phylogenies from the \emphp{control} dataset.
The orange shapes are the posterior predictive densities for the probabilities of a given phylogeny (given a divergence time estimation method and an analysis method) displaying a diversification-rate decrease.
The black stars are the observed values.}
\label{fig:control_pps}
\end{figure}
\end{comment}

%\begin{figure}[H]
%\begin{center}
%\includegraphics[width=0.9\linewidth]{./figures/predict_test_nprs.pdf}
%\end{center}
%\caption{
%Results from posterior predictive simulation for phylogenies from the \emphp{test} dataset made ultrametric using NPRS.
%Each boxplot represents the 95\% credible interval for the fraction of phylogenies displaying diversification-rate decreases for each of the methods of analysis.
%The red stars are the observed values.}
%\label{fig:predict_test_nprs}
%\end{figure}
%
%\begin{figure}[H]
%\begin{center}
%\includegraphics[width=0.9\linewidth]{./figures/predict_test_pl.pdf}
%\end{center}
%\caption{
%Results from posterior predictive simulation for phylogenies from the \emphp{test} dataset made ultrametric using PL.
%Each boxplot represents the 95\% credible interval for the fraction of phylogenies displaying diversification-rate decreases for each of the methods of analysis.
%The red stars are the observed values.}
%\label{fig:predict_test_pl}
%\end{figure}
%
%\begin{figure}[H]
%\begin{center}
%\includegraphics[width=0.9\linewidth]{./figures/predict_test_beast.pdf}
%\end{center}
%\caption{
%Results from posterior predictive simulation for phylogenies from the \emphp{test} dataset made ultrametric using BEAST.
%Each boxplot represents the 95\% credible interval for the fraction of phylogenies displaying diversification-rate decreases for each of the methods of analysis.
%The red stars are the observed values.}
%\label{fig:predict_test_beast}
%\end{figure}
%
%\begin{figure}[H]
%\begin{center}
%\includegraphics[width=0.9\linewidth]{./figures/predict_control_nprs.pdf}
%\end{center}
%\caption{
%Results from posterior predictive simulation for phylogenies from the \emphp{control} dataset made ultrametric using NPRS.
%Each boxplot represents the 95\% credible interval for the fraction of phylogenies displaying diversification-rate decreases for each of the methods of analysis.
%The red stars are the observed values.}
%\label{fig:predict_control_nprs}
%\end{figure}
%
%\begin{figure}[H]
%\begin{center}
%\includegraphics[width=0.9\linewidth]{./figures/predict_control_pl.pdf}
%\end{center}
%\caption{
%Results from posterior predictive simulation for phylogenies from the \emphp{control} dataset made ultrametric using PL.
%Each boxplot represents the 95\% credible interval for the fraction of phylogenies displaying diversification-rate decreases for each of the methods of analysis.
%The red stars are the observed values.}
%\label{fig:predict_control_pl}
%\end{figure}

\begin{figure}[H]
\begin{center}
\includegraphics[width=\textwidth]{./figures/heatmap_combined_2.pdf}
\end{center}
\caption{Heat maps of the results for each analysis method and dataset (excluding the test dataset analyzed with non-parametric rate smoothing, Figure \ref{fig:heatmap_test_nprs}).
		 For each heat map, the rows are the phylogenies, the columns are, from left to right, the $\gamma$-statistic, the $\gamma$-statistic employing the diversified model of taxon sampling, birth-death likelihood, birth-death likelihood employing the diversified model of taxon sampling, the episodic birth-death approach, and the epidemiological diversity-dependent approach. A cell is orange if that method detected a decrease in diversification on that phylogeny (diversity-dependent or otherwise), green if it detected an increase, blue if it failed to reject constant rates, and grey if the analysis failed.}
\label{fig:heatmap_combined}
\end{figure}

%\begin{figure}[H]
%\begin{center}
%\includegraphics[]{./figures/heatmap_test_pl.pdf}
%\end{center}
%\caption{Heat map of the results for each analysis method (excluding diversified sampling) on every PL \emph{test} phylogeny. The rows are the phylogenies, the columns are, from left to right, the $\gamma$ statistic, the $\gamma$ statistic employing the diversified model of taxon sampling, birth-death likelihood, birth-death likelihood employing the diversified model of taxon sampling, the episodic birth-death approach, and the epidemiological diversity-dependent approach. A cell is orange if that method detected a decrease in diversification on that phylogeny (diversity-dependent or otherwise), green if it detected an increase, blue if it failed to reject constant rates, and grey if the analysis failed.}
%\label{fig:heatmap_test_pl}
%\end{figure}
%
%\begin{figure}[H]
%\begin{center}
%\includegraphics[]{./figures/heatmap_test_beast.pdf}
%\end{center}
%\caption{Heat map of the results for each analysis method (excluding diversified sampling) on every BEAST \emph{test} phylogeny. The rows are the phylogenies, the columns are, from left to right, the $\gamma$ statistic, the $\gamma$ statistic employing the diversified model of taxon sampling, birth-death likelihood, birth-death likelihood employing the diversified model of taxon sampling, the episodic birth-death approach, and the epidemiological diversity-dependent approach. A cell is orange if that method detected a decrease in diversification on that phylogeny (diversity-dependent or otherwise), green if it detected an increase, blue if it failed to reject constant rates, and grey if the analysis failed.}
%\label{fig:heatmap_test_beast}
%\end{figure}
%
%\begin{figure}[H]
%\begin{center}
%\includegraphics[]{./figures/heatmap_control_nprs.pdf}
%\end{center}
%\caption{Heat map of the results for each analysis method (excluding diversified sampling) on every NPRS \emph{control} phylogeny. The rows are the phylogenies, the columns are, from left to right, the $\gamma$ statistic, the $\gamma$ statistic employing the diversified model of taxon sampling, birth-death likelihood, birth-death likelihood employing the diversified model of taxon sampling, the episodic birth-death approach, and the epidemiological diversity-dependent approach. A cell is orange if that method detected a decrease in diversification on that phylogeny (diversity-dependent or otherwise), green if it detected an increase, blue if it failed to reject constant rates, and grey if the analysis failed.}
%\label{fig:heatmap_control_nprs}
%\end{figure}
%
%\begin{figure}[H]
%\begin{center}
%\includegraphics[]{./figures/heatmap_control_pl.pdf}
%\end{center}
%\caption{Heat map of the results for each analysis method (excluding diversified sampling) on every PL \emph{control} phylogeny. The rows are the phylogenies, the columns are, from left to right, the $\gamma$ statistic, the $\gamma$ statistic employing the diversified model of taxon sampling, birth-death likelihood, birth-death likelihood employing the diversified model of taxon sampling, the episodic birth-death approach, and the epidemiological diversity-dependent approach. A cell is orange if that method detected a decrease in diversification on that phylogeny (diversity-dependent or otherwise), green if it detected an increase, blue if it failed to reject constant rates, and grey if the analysis failed.}
%\label{fig:heatmap_control_pl}
%\end{figure}

\end{document}