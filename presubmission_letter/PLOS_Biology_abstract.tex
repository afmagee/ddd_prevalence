\documentclass[10pt]{article}
\usepackage{cite}
\usepackage{color} 
\usepackage{times}  
\usepackage{amsmath, amsfonts, amssymb, graphicx, mathrsfs}
\usepackage{hyperref}
\usepackage[table]{xcolor}

% Margins
\topmargin=-1in % Moves the top of the document 1 inch above the default
\textheight=8.5in % Total height of the text on the page before text goes on to the next page, this can be increased in a longer letter
\oddsidemargin=-10pt % Position of the left margin, can be negative or positive if you want more or less room
\textwidth=6.5in % Total width of the text, increase this if the left margin was decreased and vice-versa

% Use the PLoS provided bibtex style
\bibliographystyle{plos2015}

% Remove brackets from numbering in List of References
\makeatletter
\renewcommand{\@biblabel}[1]{\quad#1.}
\makeatother

\pagestyle{myheadings}
% ** EDIT HERE **
\usepackage[square]{natbib}
\usepackage{rotating}
\usepackage{booktabs}
\usepackage{colortbl}
\usepackage[table]{xcolor}
\usepackage{float}
\usepackage{comment}
\usepackage{multirow}
\usepackage{paralist}
\usepackage{xspace}

% ** EDIT HERE **
% PLEASE INCLUDE ALL MACROS BELOW
\newcommand{\IE}{{\it i.e.,}\xspace}
\newcommand{\EG}{{\it e.g.,}\xspace}
\newcommand{\CF}{{\it c.f.,}\xspace}
\newcommand{\laser}{\texttt{laser}\xspace}
\newcommand{\expotree}{\texttt{expotree}\xspace}
%\input{preamble.tex}


\newcommand{\levelone}[1]{%
\bigskip
\begin{center}
\begin{Large}
\normalfont\scshape #1
%\medskip
\end{Large}
\end{center}}

\newcommand{\leveltwo}[1]{%
%\bigskip
\begin{center}
\begin{large}
\normalfont\itshape #1
\end{large}
\end{center}}

\newcommand{\levelthree}[1]{%
\vspace{2ex}
\noindent
\textit{#1.}---}

\begin{document}
%\begin{flushleft}

\begin{flushleft}
\textbf{The Empirical Prevalence of Diversity-Dependent Diversification and the Factors Influencing its Detection}

\medskip
Diversity-dependent diversification posits that the net-diversification rate (speciation -- extinction) of a lineage decreases through time owing to ecological interactions among its member species.
This hypothesis invokes an ecological carrying capacity for each lineage: the species in a growing lineage eventually exhaust available ecological opportunities (thereby decreasing the probability of speciation), and/or increase the intensity of competitive ecological interactions (thereby increasing the probability of extinction).
The study of diversity-dependent diversification continues to be an exceptionally active locus of research, including the development (and scrutiny) of statistical methods, the analysis of empirical data, and the elaboration of ecological and evolutionary theory.
%\end{flushleft}

\medskip
Several statistical methods have been developed to detect diversity-dependent diversification from phylogenetic trees inferred from molecular sequence data \citep[{\it e.g.},][]{paradis1997, paradis2004, pybus2000, rabosky2006bdl, rabosky2006laser, Morlon2010, Morlon2011, Morlon2015, Stadler2011, Etienne2012a, Etienne2012b, Leventhal2014, May2016, Hohna2016}.
In general, these approaches involve evaluating the relative fit of the phylogenetic `observations' (the vector of branching times in the estimated tree) to alternative stochastic-branching process models.
For example, we can compare the relative fit of a given tree to a model in which diversification rates are constant through time to an \textit{implicit} diversity-dependent diversification model \citep[where diversification rates decrease through time;][]{paradis1997, paradis2004, pybus2000, Stadler2011, Hohna2016} or to an \textit{explicit} diversity-dependent diversification model \citep[where diversification rates are a function of the number of species in the tree;][]{rabosky2006bdl, rabosky2006laser, Morlon2010, Morlon2011, Morlon2015, Etienne2012a, Leventhal2014}.  

\medskip
Additional methodological research in this area has identified factors that may cause the prevalence of diversity-dependent diversification to be overestimated.
Spurious detection of diversity-dependent diversification may be caused both by \textit{phylogenetic} biases (associated with the procedure used to estimate study phylogenies) and also by \textit{methodological} biases (associated with the statistical approaches used to detect diversity-dependent diversification in study phylogenies).
Phylogenetic biases include the use of an underspecified substitution model \citep[][]{revell2005} to estimate the study tree, and the inclusion of an incomplete \citep[][]{pybus2000} and/or non-random \citep[][]{brock2011,cusimano2012} sample of species in the phylogenetic analysis.
Methodological biases include the use of the likelihood-ratio test to select among stochastic-branching process models has been shown to be biased toward more complex (diversity-dependent) models \citep[][]{etienne2016}, and by violation of the assumptions of the statistical methods \citep[{\it e.g.}, variation in diversification rates across lineages of the study tree][]{pybus2000}.

\medskip
Several studies have applied these statistical methods to large samples of empirical trees to assess the prevalence of diversity-dependent diversification: (1) McPeek \citep[][]{mcpeek2008} applied the gamma statistic \citep[][]{pybus2000} to a sample of 240 trees; (2) similarly, Phillimore and Price \citep[][]{phillimore2008} applied the gamma statistic to a sample of 45 trees, and; (3) Morlon \emph{et al.} \citep[][]{Morlon2010} applied their coalescent-based statistical approach \citep[][]{Morlon2011, Morlon2015} to the set of trees from the two previous studies.
These empirical surveys have generally substantiated theoretical arguments \citep[{\it e.g.},][]{sepkoski1978, levinton1979, rabosky2013, etienne2011} predicting that diversity-dependent diversification should be a pervasive feature of empirical phylogenies.

\medskip
Accordingly, it is widely accepted that diversity-dependent diversification either \textit{is} a pervasive feature of empirical phylogenies, or that an empirical survey \textit{should} reveal diversity-dependent diversification to be pervasive, even if the detection rate for diversity-dependent diversification is somewhat inflated by methodological artifacts.
Here, we explore the prevalence of diversity-dependent diversification by means of an empirical survey that greatly increases both the sampling intensity (comprising a sample of 391 trees) and methodological diversity (incorporating four statistical methods) of previous efforts.
Our survey reveals tremendous uncertainty regarding the empirical prevalence of diversity-dependent diversification (the detection rate in the sampled trees ranges from 4 to 71\%).
We explore the source of this uncertainty by performing a Bayesian logistic regression analysis that reveals the factors that must be overcome in order to understand the extent to which diversity-dependent diversification has shaped the Tree of Life.

\bibliography{prevalence.bib}
\end{document}